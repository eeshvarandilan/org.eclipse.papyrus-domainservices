/*******************************************************************************
 * Copyright (c) 2022 CEA, Obeo.
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Obeo - initial API and implementation
 *******************************************************************************/
package org.eclipse.papyrus.uml.domain.services.utils;

import static org.junit.jupiter.api.Assertions.fail;

import java.util.List;
import java.util.Optional;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.util.ECrossReferenceAdapter;
import org.eclipse.papyrus.uml.domain.services.EMFUtils;
import org.eclipse.papyrus.uml.domain.services.IEditableChecker;
import org.eclipse.uml2.uml.NamedElement;
import org.eclipse.uml2.uml.UMLFactory;
import org.eclipse.uml2.uml.UMLPackage;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;

/**
 * Abstract test class offering util methods to create UML models.
 * 
 * @author Arthur Daussy
 *
 */
public class AbstractUMLTest {

    private ECrossReferenceAdapter crossRef;

    private IEditableChecker editableChecker;

    @BeforeEach
    public void setUp() {
        this.crossRef = new ECrossReferenceAdapter();
        this.editableChecker = e -> true;
    }

    public IEditableChecker getEditableChecker() {
        return this.editableChecker;
    }

    public ECrossReferenceAdapter getCrossRef() {
        return this.crossRef;
    }

    /**
     * Creates an element with the given type in the given parent. The containment
     * reference is automatically computed by finding the feature containment
     * {@link EReference} that can contains the given object.
     * 
     * @param <T>
     *               the expected type of the given element
     * @param type
     *               the expected type of the given element
     * @param parent
     *               the container
     * @return a new element
     */
    protected <T extends EObject> T createIn(java.lang.Class<T> type, EObject parent) {
        Optional<EReference> defaultContainementRef = parent.eClass().getEAllContainments().stream()
                .filter(ref -> ref.getEType().getInstanceClass().isAssignableFrom(type)).findFirst();

        if (defaultContainementRef.isPresent()) {
            return createIn(type, parent, defaultContainementRef.get().getName());
        } else {
            Assertions.fail("Unable to find a containement reference for " + type.getSimpleName() + " in " + parent);
            return null;
        }
    }

    @SuppressWarnings({ "unchecked", "rawtypes" })
    protected <T extends EObject> T createIn(java.lang.Class<T> type, EObject parent, String containmentRefName) {
        T newElement = create(type);

        EStructuralFeature ref = parent.eClass().getEStructuralFeature(containmentRefName);
        if (ref == null || !(ref instanceof EReference)) {
            Assertions.fail("Invalid reference name");
        }

        if (ref.isDerived()) {
            Assertions.fail(ref.getName() + " is a derived feature.");
        }
        EReference eRef = (EReference) ref;
        if (!eRef.getEType().isInstance(newElement)) {
            fail("Invalid reference " + eRef.getName() + " for element" + newElement);
        }
        if (ref.isMany()) {
            ((List) parent.eGet(ref)).add(newElement);
        } else {
            parent.eSet(ref, newElement);
        }

        return newElement;
    }

    /**
     * Create an UML element.
     * 
     * @param <T>
     *             type of element
     * @param type
     *             of element
     * @return created element
     */
    @SuppressWarnings("unchecked")
    protected <T extends EObject> T create(java.lang.Class<T> type) {

        EClassifier newType = UMLPackage.eINSTANCE.getEClassifier(type.getSimpleName());
        EObject newElement = UMLFactory.eINSTANCE.create((EClass) newType);

        if (!newElement.eAdapters().contains(this.crossRef)) {
            newElement.eAdapters().add(this.crossRef);
        }
        return (T) newElement;
    }

    /**
     * Create an UML named element with provided name.
     * 
     * @param <T>
     *             type of element
     * @param type
     *             of element
     * @param name
     *             of element
     * @return created element
     */
    protected <T extends NamedElement> T createNamedElement(java.lang.Class<T> type, String name) {
        T result = create(type);
        result.setName(name);
        return result;
    }

    /**
     * Get an element from a containment tree.
     * 
     * @param owner
     *              root element of the tree
     * @param type
     *              type of searched element
     * @param name
     *              filter on the NamedElement.name feature. It is ignored if null;
     * @return the first element matching the filters.
     */
    protected <T extends EObject> Optional<T> getElement(EObject owner, java.lang.Class<T> type, String name) {
        Optional<T> element = EMFUtils.allContainedObjectOfType(owner, type)//
                .filter(type::isInstance).map(type::cast).filter(object -> {
                    boolean keep = true;
                    if (name != null) {
                        keep = object instanceof NamedElement && name.equals(((NamedElement) object).getName());
                    }
                    return keep;
                })//
                .findFirst();
        return element;
    }
}
