/*****************************************************************************
 * Copyright (c) 2023 CEA LIST, Obeo.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Obeo - Initial API and implementation
 *****************************************************************************/
package org.eclipse.papyrus.uml.domain.services.drop;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.stream.Stream;

import org.eclipse.papyrus.uml.domain.services.drop.diagrams.ComponentInternalSourceToRepresentationDropChecker;
import org.eclipse.papyrus.uml.domain.services.status.CheckStatus;
import org.eclipse.papyrus.uml.domain.services.utils.AbstractUMLTest;
import org.eclipse.uml2.uml.Comment;
import org.eclipse.uml2.uml.Component;
import org.eclipse.uml2.uml.Constraint;
import org.eclipse.uml2.uml.Deployment;
import org.eclipse.uml2.uml.Element;
import org.eclipse.uml2.uml.Interface;
import org.eclipse.uml2.uml.Operation;
import org.eclipse.uml2.uml.Package;
import org.eclipse.uml2.uml.Property;
import org.eclipse.uml2.uml.Reception;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

/**
 * Test class for {@link ComponentInternalSourceToRepresentationDropChecker}.
 *
 * @author <a href="mailto:gwendal.daniel@obeosoft.com">Gwendal Daniel</a>
 */
public class ComponentInternalSourceToRepresentationDropCheckerTest extends AbstractUMLTest {

    private ComponentInternalSourceToRepresentationDropChecker componentInternalSourceToRepresentationDropChecker;

    @BeforeEach
    @Override
    public void setUp() {
        super.setUp();
        this.componentInternalSourceToRepresentationDropChecker = new ComponentInternalSourceToRepresentationDropChecker();
    }

    @ParameterizedTest
    @MethodSource("getCanDragAndDropParameters")
    public void testCanDragAndDrop(Class<? extends Element> elementToDropClazz,
            Class<? extends Element> semanticContainerClazz, boolean expectedCheckStatus) {
        Element elementToDrop = this.create(elementToDropClazz);
        Element semanticContainer = this.create(semanticContainerClazz);

        CheckStatus canDragAndDropStatus = this.componentInternalSourceToRepresentationDropChecker
                .canDragAndDrop(elementToDrop, semanticContainer);
        assertEquals(expectedCheckStatus, canDragAndDropStatus.isValid());
    }

    /**
     * Tests that a property can be drag and drop from a component to another
     * component.
     */
    @Test
    public void testCanDragAndDropPropertyFromComponentToComponent() {
        Property property = this.create(Property.class);
        Component component = this.create(Component.class);
        component.getOwnedAttributes().add(property);
        Component newComponent = this.create(Component.class);

        CheckStatus canDragAndDropStatus = this.componentInternalSourceToRepresentationDropChecker
                .canDragAndDrop(property, newComponent);
        assertTrue(canDragAndDropStatus.isValid());
    }

    /**
     * Tests that a property can't be drag and drop from a component to a
     * non-component.
     */
    @Test
    public void testCanDragAndDropPropertyFromComponentToNonComponent() {
        Property property = this.create(Property.class);
        Component component = this.create(Component.class);
        component.getOwnedAttributes().add(property);
        Interface interfaceElement = this.create(Interface.class);

        CheckStatus canDragAndDropStatus = this.componentInternalSourceToRepresentationDropChecker
                .canDragAndDrop(property, interfaceElement);
        assertFalse(canDragAndDropStatus.isValid());
    }

    /**
     * Tests that a property can be drag and drop from a component to a typed
     * property.
     */
    @Test
    public void testCanDragAndDropPropertyFromComponentToTypedProperty() {
        Property property = this.create(Property.class);
        Component component = this.create(Component.class);
        component.getOwnedAttributes().add(property);

        Property typedProperty = this.create(Property.class);
        Interface interfaceElement = this.create(Interface.class);
        typedProperty.setType(interfaceElement);

        CheckStatus canDragAndDropStatus = this.componentInternalSourceToRepresentationDropChecker
                .canDragAndDrop(property, typedProperty);
        assertTrue(canDragAndDropStatus.isValid());
    }

    /**
     * Tests that a property can be drag and drop from an interface to another
     * interface.
     */
    @Test
    public void testCanDragAndDropPropertyFromInterfaceToInterface() {
        Property property = this.create(Property.class);
        Interface containingInterface = this.create(Interface.class);
        property.setInterface(containingInterface);
        Interface newInterface = this.create(Interface.class);

        CheckStatus canDragAndDropStatus = this.componentInternalSourceToRepresentationDropChecker
                .canDragAndDrop(property, newInterface);
        assertTrue(canDragAndDropStatus.isValid());
    }

    /**
     * Tests that a property can't be drag and drop from an interface to a
     * non-interface.
     */
    @Test
    public void testCanDragAndDropPropertyFromInterfaceToNonInterface() {
        Property property = this.create(Property.class);
        Interface containingInterface = this.create(Interface.class);
        property.setInterface(containingInterface);
        Component component = this.create(Component.class);

        CheckStatus canDragAndDropStatus = this.componentInternalSourceToRepresentationDropChecker
                .canDragAndDrop(property, component);
        assertFalse(canDragAndDropStatus.isValid());
    }

    public static Stream<Arguments> getCanDragAndDropParameters() {
        // @formatter:off
        return Stream.of(
                Arguments.of(Comment.class, Comment.class, false),
                Arguments.of(Comment.class, Package.class, true),
                Arguments.of(Component.class, Comment.class, false),
                Arguments.of(Component.class, Component.class, true),
                Arguments.of(Component.class, Package.class, true),
                Arguments.of(Constraint.class, Comment.class, false),
                Arguments.of(Constraint.class, Package.class, true),
                Arguments.of(Interface.class, Comment.class, false),
                Arguments.of(Interface.class, Package.class, true),
                Arguments.of(Operation.class, Comment.class, false),
                Arguments.of(Operation.class, Interface.class, true),
                Arguments.of(Package.class, Comment.class, false),
                Arguments.of(Package.class, Package.class, true),
                Arguments.of(Property.class, Comment.class, false),
                Arguments.of(Reception.class, Comment.class, false),
                Arguments.of(Reception.class, Interface.class, true),
                Arguments.of(Deployment.class, Package.class, false)
                );
        // @formatter:on
    }

}
