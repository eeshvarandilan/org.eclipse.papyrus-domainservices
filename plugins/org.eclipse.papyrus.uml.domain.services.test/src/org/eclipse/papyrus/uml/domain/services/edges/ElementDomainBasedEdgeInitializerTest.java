/*****************************************************************************
 * Copyright (c) 2022, 2023 CEA LIST, Obeo.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Obeo - Initial API and implementation
 *****************************************************************************/

package org.eclipse.papyrus.uml.domain.services.edges;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.List;
import java.util.stream.Collectors;

import org.eclipse.emf.common.util.EMap;
import org.eclipse.papyrus.uml.domain.services.utils.AbstractUMLTest;
import org.eclipse.uml2.common.util.UML2Util;
import org.eclipse.uml2.uml.Activity;
import org.eclipse.uml2.uml.ActivityNode;
import org.eclipse.uml2.uml.ActivityPartition;
import org.eclipse.uml2.uml.Actor;
import org.eclipse.uml2.uml.AggregationKind;
import org.eclipse.uml2.uml.Artifact;
import org.eclipse.uml2.uml.Association;
import org.eclipse.uml2.uml.Class;
import org.eclipse.uml2.uml.Component;
import org.eclipse.uml2.uml.ComponentRealization;
import org.eclipse.uml2.uml.ControlFlow;
import org.eclipse.uml2.uml.Dependency;
import org.eclipse.uml2.uml.Deployment;
import org.eclipse.uml2.uml.Extend;
import org.eclipse.uml2.uml.Extension;
import org.eclipse.uml2.uml.ExtensionPoint;
import org.eclipse.uml2.uml.ForkNode;
import org.eclipse.uml2.uml.Generalization;
import org.eclipse.uml2.uml.Include;
import org.eclipse.uml2.uml.InformationFlow;
import org.eclipse.uml2.uml.InputPin;
import org.eclipse.uml2.uml.Interface;
import org.eclipse.uml2.uml.InterfaceRealization;
import org.eclipse.uml2.uml.JoinNode;
import org.eclipse.uml2.uml.Manifestation;
import org.eclipse.uml2.uml.Node;
import org.eclipse.uml2.uml.ObjectFlow;
import org.eclipse.uml2.uml.OpaqueAction;
import org.eclipse.uml2.uml.OutputPin;
import org.eclipse.uml2.uml.Package;
import org.eclipse.uml2.uml.PackageImport;
import org.eclipse.uml2.uml.PackageMerge;
import org.eclipse.uml2.uml.Property;
import org.eclipse.uml2.uml.Stereotype;
import org.eclipse.uml2.uml.Substitution;
import org.eclipse.uml2.uml.UseCase;
import org.junit.jupiter.api.Test;

/**
 * Test class for {@link ElementDomainBasedEdgeInitializer}.
 *
 * @author <a href="mailto:arthur.daussy@obeo.fr">Arthur Daussy</a>
 *
 */
public class ElementDomainBasedEdgeInitializerTest extends AbstractUMLTest {

    private static final String META_CLASS = "metaClass";
    private static final String STEREOTYPE = "stereotype";
    private static final String NATURE = "nature";
    private static final String EXTENSION = "extension_";
    private static final String BASE = "base_";

    /**
     * Basic test case for {@link Association} initialization.
     */
    @Test
    public void testAssociation() {
        Actor source = this.create(Actor.class);
        source.setName("Actor1");
        Actor target = this.create(Actor.class);
        target.setName("Actor2");
        Association element = this.create(Association.class);

        EMap<String, String> details = UML2Util.getEAnnotation(element, "org.eclipse.papyrus", true).getDetails();

        new ElementDomainBasedEdgeInitializer().initialize(element, source, target, null, null, null);

        // Check Nature
        assertTrue(details.containsKey(NATURE));
        assertEquals("UML_Nature", details.get(NATURE));

        List<Property> propList = element.getMemberEnds().stream().filter(Property.class::isInstance)
                .map(Property.class::cast).collect(Collectors.toList());

        assertEquals(target.getName().toLowerCase(), propList.get(0).getName());
        assertEquals(source.getName().toLowerCase(), propList.get(1).getName());

    }

    /**
     * Basic test case for {@link Association} initialization with an existing
     * EAnnotation key "nature". Expected : The key is remove and recreate.
     */
    @Test
    public void testAssociationWithExistingEAnnotationKey() {
        Actor source = this.create(Actor.class);
        source.setName("Actor1");
        Actor target = this.create(Actor.class);
        target.setName("Actor2");
        Association element = this.create(Association.class);

        EMap<String, String> details = UML2Util.getEAnnotation(element, "org.eclipse.papyrus", true).getDetails();

        details.put(NATURE, "already exist");
        new ElementDomainBasedEdgeInitializer().initialize(element, source, target, null, null, null);
        assertEquals("UML_Nature", details.get(NATURE));
    }

    /**
     * Basic test case for {@link ComponentRealization}.
     */
    @Test
    public void testComponentRealization() {
        Interface sourceInterface = this.create(Interface.class);
        Component targetComponent = this.create(Component.class);
        ComponentRealization componentRealization = this.create(ComponentRealization.class);

        new ElementDomainBasedEdgeInitializer().initialize(componentRealization, sourceInterface, targetComponent, null,
                null, null);

        assertTrue(componentRealization.getRealizingClassifiers().contains(sourceInterface));
        assertEquals(targetComponent, componentRealization.getAbstraction());
    }

    @Test
    public void testControlFlow() {
        ControlFlow controlFlow = this.create(ControlFlow.class);
        ForkNode forkNode = this.create(ForkNode.class);
        JoinNode joinNode = this.create(JoinNode.class);
        new ElementDomainBasedEdgeInitializer().initialize(controlFlow, forkNode, joinNode, null, null, null);
        assertEquals(forkNode, controlFlow.getSource());
        assertEquals(joinNode, controlFlow.getTarget());
    }

    @Test
    public void testControlFlowInPartition() {
        ControlFlow controlFlow = this.create(ControlFlow.class);
        Activity activity = this.create(Activity.class);
        ActivityPartition activityPartition = this.createIn(ActivityPartition.class, activity);
        ForkNode forkNode = this.createIn(ForkNode.class, activity);
        forkNode.getInPartitions().add(activityPartition);
        JoinNode joinNode = this.createIn(JoinNode.class, activity);
        joinNode.getInPartitions().add(activityPartition);
        new ElementDomainBasedEdgeInitializer().initialize(controlFlow, forkNode, joinNode, null, null, null);
        assertEquals(forkNode, controlFlow.getSource());
        assertEquals(joinNode, controlFlow.getTarget());
        assertEquals(1, controlFlow.getInPartitions().size());
        assertEquals(activityPartition, controlFlow.getInPartitions().get(0));
    }

    @Test
    public void testControlFlowWithNullValues() {
        ControlFlow controlFlow = this.create(ControlFlow.class);
        new ElementDomainBasedEdgeInitializer().initialize(controlFlow, null, null, null, null, null);
        assertNull(controlFlow.getSource());
        assertNull(controlFlow.getTarget());
    }

    /**
     * Basic test case for Dependency initialization.
     */
    @Test
    public void testDependency() {
        Class c1 = this.create(Class.class);
        Class c2 = this.create(Class.class);
        Dependency dependency = this.create(Dependency.class);

        new ElementDomainBasedEdgeInitializer().initialize(dependency, c1, c2, null, null, null);

        assertEquals(c1, dependency.getClients().get(0));
        assertEquals(c2, dependency.getSuppliers().get(0));
    }

    /**
     * Basic test case for {@link Deployment}.
     */
    @Test
    public void testDeployment() {
        Artifact sourceArtifact = this.create(Artifact.class);
        Node targetNode = this.create(Node.class);
        Deployment deployment = this.create(Deployment.class);

        new ElementDomainBasedEdgeInitializer().initialize(deployment, sourceArtifact, targetNode, null, null, null);

        assertTrue(deployment.getDeployedArtifacts().contains(sourceArtifact));
        assertEquals(1, deployment.getSuppliers().size());
        assertTrue(deployment.getSuppliers().contains(sourceArtifact));
        assertEquals(targetNode, deployment.getLocation());
        assertEquals(1, deployment.getClients().size());
        assertTrue(deployment.getClients().contains(targetNode));
    }

    /**
     * Basic test case for {@link Extend} initialization.
     */
    @Test
    public void testExtend() {
        Extend extend = this.create(Extend.class);
        UseCase source = this.create(UseCase.class);
        UseCase target = this.create(UseCase.class);

        new ElementDomainBasedEdgeInitializer().initialize(extend, source, target, null, null, null);

        assertEquals(source, extend.getExtension());
        assertEquals(target, extend.getExtendedCase());
        ExtensionPoint extensionPoint = extend.getExtensionLocations().get(0);
        assertNotNull(extensionPoint);
    }

    /**
     * Basic test case for {@link Extension} initialization.
     */
    @Test
    public void testExtension() {
        Stereotype stereotype = this.create(Stereotype.class);
        stereotype.setName(STEREOTYPE);
        Class class1 = this.create(Class.class);
        class1.setName(META_CLASS);
        Extension extension = this.create(Extension.class);
        new ElementDomainBasedEdgeInitializer().initialize(extension, stereotype, class1, null, null, null);
        assertEquals(stereotype, extension.getStereotype());
        assertEquals(class1, extension.getMetaclass());

        assertEquals(1, extension.getOwnedEnds().size());
        Property extensionEnd = extension.getOwnedEnds().get(0);
        assertEquals(EXTENSION + STEREOTYPE, extensionEnd.getName());
        assertEquals(AggregationKind.COMPOSITE_LITERAL, extensionEnd.getAggregation());

        assertEquals(1, stereotype.getOwnedAttributes().size());
        Property property = stereotype.getOwnedAttributes().get(0);
        assertEquals(BASE + META_CLASS, property.getName());
        assertEquals(AggregationKind.NONE_LITERAL, property.getAggregation());
        assertEquals(0, property.getLower());
    }

    /**
     * Basic test case for Generalization initialization.
     */
    @Test
    public void testGeneralization() {
        Class c1 = this.create(Class.class);
        Class c2 = this.create(Class.class);
        Generalization generalization = this.create(Generalization.class);

        new ElementDomainBasedEdgeInitializer().initialize(generalization, c1, c2, null, null, null);

        assertEquals(c1, generalization.getSpecific());
        assertEquals(c2, generalization.getGeneral());

    }

    /**
     * Basic test case for {@link Include} initialization.
     */
    @Test
    public void testInclude() {
        Include include = this.create(Include.class);
        UseCase source = this.create(UseCase.class);
        UseCase target = this.create(UseCase.class);

        new ElementDomainBasedEdgeInitializer().initialize(include, source, target, null, null, null);

        assertEquals(source, include.getIncludingCase());
        assertEquals(target, include.getAddition());

    }

    /**
     * Basic test case for {@link InformationFlow} initialization.
     */
    @Test
    public void testInformationFlow() {
        Class c1 = this.create(Class.class);
        Class c2 = this.create(Class.class);
        InformationFlow informationFlow = this.create(InformationFlow.class);

        new ElementDomainBasedEdgeInitializer().initialize(informationFlow, c1, c2, null, null, null);

        assertEquals(c1, informationFlow.getInformationSources().get(0));
        assertEquals(c2, informationFlow.getInformationTargets().get(0));

    }

    /**
     * Basic test case for {@link InterfaceRealization}.
     */
    @Test
    public void testInterfaceRealization() {
        Class sourceClassifier = this.create(Class.class);
        Interface targetInteface = this.create(Interface.class);
        InterfaceRealization interfaceRealization = this.create(InterfaceRealization.class);

        new ElementDomainBasedEdgeInitializer().initialize(interfaceRealization, sourceClassifier, targetInteface, null,
                null, null);

        assertEquals(sourceClassifier, interfaceRealization.getImplementingClassifier());
        assertEquals(targetInteface, interfaceRealization.getContract());

    }

    /**
     * Basic test case for {@link Manifestation} initialization.
     */
    @Test
    public void testManifestation() {
        Class c1 = this.create(Class.class);
        Class c2 = this.create(Class.class);
        Manifestation manifestation = this.create(Manifestation.class);

        new ElementDomainBasedEdgeInitializer().initialize(manifestation, c1, c2, null, null, null);

        assertEquals(c1, manifestation.getClients().get(0));
        assertEquals(c2, manifestation.getUtilizedElement());
        assertEquals(c2, manifestation.getSuppliers().get(0));

    }

    @Test
    public void testObjectFlow() {
        ObjectFlow objectFlow = this.create(ObjectFlow.class);
        ForkNode forkNode = this.create(ForkNode.class);
        JoinNode joinNode = this.create(JoinNode.class);
        new ElementDomainBasedEdgeInitializer().initialize(objectFlow, forkNode, joinNode, null, null, null);
        assertEquals(forkNode, objectFlow.getSource());
        assertEquals(joinNode, objectFlow.getTarget());
    }

    @Test
    public void testObjectFlowInPartition() {
        ObjectFlow objectFlow = this.create(ObjectFlow.class);
        Activity activity = this.create(Activity.class);
        ActivityPartition activityPartition = this.createIn(ActivityPartition.class, activity);
        ForkNode forkNode = this.createIn(ForkNode.class, activity);
        forkNode.getInPartitions().add(activityPartition);
        JoinNode joinNode = this.createIn(JoinNode.class, activity);
        joinNode.getInPartitions().add(activityPartition);
        new ElementDomainBasedEdgeInitializer().initialize(objectFlow, forkNode, joinNode, null, null, null);
        assertEquals(forkNode, objectFlow.getSource());
        assertEquals(joinNode, objectFlow.getTarget());
        assertEquals(1, objectFlow.getInPartitions().size());
        assertEquals(activityPartition, objectFlow.getInPartitions().get(0));
    }

    @Test
    public void testObjectFlowWithNullValues() {
        ObjectFlow objectFlow = this.create(ObjectFlow.class);
        new ElementDomainBasedEdgeInitializer().initialize(objectFlow, null, null, null, null, null);
        assertNull(objectFlow.getSource());
        assertNull(objectFlow.getTarget());
    }

    @Test
    public void testObjectFlowWithOpaqueAction() {
        ObjectFlow objectFlow = this.create(ObjectFlow.class);
        OpaqueAction source = this.create(OpaqueAction.class);
        OpaqueAction target = this.create(OpaqueAction.class);
        new ElementDomainBasedEdgeInitializer().initialize(objectFlow, source, target, null, null, null);
        ActivityNode newSource = objectFlow.getSource();
        ActivityNode newTarget = objectFlow.getTarget();
        assertTrue(newSource instanceof OutputPin);
        assertTrue(newTarget instanceof InputPin);
        assertTrue(newSource.eContainer().equals(source));
        assertTrue(newTarget.eContainer().equals(target));
    }

    /**
     * An {@link ObjectFlow} created on {@link OpaqueAction} should create
     * {@link InputPin} and {@link OutputPin}, each Pin should be added to the
     * {@link ActivityPartition} if its {@link OpaqueAction} has an
     * {@link ActivityPartition}.
     */
    @Test
    public void testObjectFlowWithOpaqueActionsInPartition() {
        Activity root = this.create(Activity.class);
        ActivityPartition partition1 = this.createIn(ActivityPartition.class, root);
        ObjectFlow objectFlow = this.create(ObjectFlow.class);
        OpaqueAction source = this.createIn(OpaqueAction.class, root);
        OpaqueAction target = this.createIn(OpaqueAction.class, root);
        source.getInPartitions().add(partition1);
        target.getInPartitions().add(partition1);

        new ElementDomainBasedEdgeInitializer().initialize(objectFlow, source, target, null, null, null);
        ActivityNode newSource = objectFlow.getSource();
        ActivityNode newTarget = objectFlow.getTarget();
        assertTrue(newSource instanceof OutputPin);
        assertTrue(newTarget instanceof InputPin);
        assertTrue(objectFlow.getInPartitions().contains(partition1));
        assertTrue(source.getInPartitions().contains(partition1));
        assertTrue(target.getInPartitions().contains(partition1));
        assertTrue(newSource.getInPartitions().contains(partition1));
        assertTrue(newTarget.getInPartitions().contains(partition1));
    }

    /**
     * Basic test case for {@link PackageImport} initialization.
     */
    @Test
    public void testPackageImport() {
        Package source = this.create(Package.class);
        Package target = this.create(Package.class);
        PackageImport element = this.create(PackageImport.class);

        new ElementDomainBasedEdgeInitializer().initialize(element, source, target, null, null, null);

        assertEquals(source, element.getImportingNamespace());
        assertEquals(target, element.getImportedPackage());
    }

    /**
     * Basic test case for {@link PackageMerge} initialization.
     */
    @Test
    public void testPackageMerge() {
        Package source = this.create(Package.class);
        Package target = this.create(Package.class);
        PackageMerge element = this.create(PackageMerge.class);

        new ElementDomainBasedEdgeInitializer().initialize(element, source, target, null, null, null);

        assertEquals(source, element.getReceivingPackage());
        assertEquals(target, element.getMergedPackage());
    }

    /**
     * Basic test case for {@link Substitution} initialization.
     */
    @Test
    public void testSubstitution() {
        Class c1 = this.create(Class.class);
        Class c2 = this.create(Class.class);
        Substitution substitution = this.create(Substitution.class);

        new ElementDomainBasedEdgeInitializer().initialize(substitution, c1, c2, null, null, null);

        assertEquals(c1, substitution.getSubstitutingClassifier());
        assertEquals(c1, substitution.getClients().get(0));
        assertEquals(c2, substitution.getContract());
        assertEquals(c2, substitution.getSuppliers().get(0));

    }
}
