/*******************************************************************************
 * Copyright (c) 2022 CEA, Obeo
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *    Obeo - initial API and implementation
 *******************************************************************************/
package org.eclipse.papyrus.uml.domain.services.internal.helpers;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.List;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EcoreFactory;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.xmi.XMIResource;
import org.eclipse.emf.ecore.xmi.impl.XMIResourceImpl;
import org.eclipse.uml2.uml.Class;
import org.eclipse.uml2.uml.Component;
import org.eclipse.uml2.uml.Package;
import org.eclipse.uml2.uml.UMLFactory;
import org.eclipse.uml2.uml.internal.resource.UMLResourceImpl;
import org.eclipse.uml2.uml.resource.UMLResource;
import org.junit.jupiter.api.Test;

@SuppressWarnings("restriction")
public class UMLServiceTests {

    private static final String CLASS_TYPE = "Class"; //$NON-NLS-1$

    private UMLFactory fact = UMLFactory.eINSTANCE;

    private UMLService service = new UMLService();

    @Test
    public void testGetAllReachable() {
        ResourceSet rs = new ResourceSetImpl();

        UMLResource r1 = new UMLResourceImpl(URI.createURI("fake://1")); //$NON-NLS-1$
        rs.getResources().add(r1);

        Package pack = this.fact.createPackage();
        r1.getContents().add(pack);

        assertTrue(this.service.getAllReachable(pack, CLASS_TYPE).isEmpty());

        // Add one class, one component and one interface in the same resource

        Component cmp1 = this.fact.createComponent();
        pack.getPackagedElements().add(cmp1);
        pack.getPackagedElements().add(this.fact.createInterface());
        Class clazz = this.fact.createClass();
        pack.getPackagedElements().add(clazz);

        // Should return the clazz and the component (which inherit from class)
        assertEquals(List.of(cmp1, clazz), this.service.getAllReachable(pack, CLASS_TYPE));

        // Add one more UML resource
        UMLResource r2 = new UMLResourceImpl(URI.createURI("fake://2")); //$NON-NLS-1$
        rs.getResources().add(r2);

        Class clazz2 = this.fact.createClass();
        r2.getContents().add(clazz2);
        rs.getResources().add(r2);

        assertEquals(List.of(cmp1, clazz, clazz2), this.service.getAllReachable(pack, CLASS_TYPE));

        // Add one ecore resource

        XMIResource r3 = new XMIResourceImpl(URI.createURI("fake://3")); //$NON-NLS-1$

        r3.getContents().add(EcoreFactory.eINSTANCE.createEPackage());
        assertEquals(List.of(cmp1, clazz, clazz2), this.service.getAllReachable(pack, CLASS_TYPE));

    }

}
