/*****************************************************************************
 * Copyright (c) 2023 CEA LIST, Obeo.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Obeo - Initial API and implementation
 *****************************************************************************/
package org.eclipse.papyrus.uml.domain.services.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.List;

import org.eclipse.papyrus.uml.domain.services.internal.helpers.ActivityEdgeHelper;
import org.eclipse.papyrus.uml.domain.services.utils.AbstractUMLTest;
import org.eclipse.uml2.uml.Activity;
import org.eclipse.uml2.uml.DecisionNode;
import org.eclipse.uml2.uml.ObjectFlow;
import org.eclipse.uml2.uml.StructuredActivityNode;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

/**
 * Tests for the {@link ActivityEdgeHelper}.
 *
 * @author <a href="mailto:gwendal.daniel@obeosoft.com">Gwendal Daniel</a>
 */
public class ActivityEdgeHelperTest extends AbstractUMLTest {

    private ActivityEdgeHelper activityEdgeHelper;

    @Override
    @BeforeEach
    public void setUp() {
        super.setUp();
        this.activityEdgeHelper = new ActivityEdgeHelper();
    }

    /**
     * Tests that the activity container of an edge isn't updated if its source and
     * target elements are contained in the edge's container.
     */
    @Test
    public void testUpdateActivityEdgeContainerSourceAndTargetInEdgeActivity() {
        ObjectFlow objectFlow = this.create(ObjectFlow.class);
        DecisionNode sourceDecisionNode = this.create(DecisionNode.class);
        DecisionNode targetDecisionNode = this.create(DecisionNode.class);
        Activity activity = this.create(Activity.class);
        activity.getEdges().add(objectFlow);
        sourceDecisionNode.setActivity(activity);
        targetDecisionNode.setActivity(activity);
        objectFlow.setSource(sourceDecisionNode);
        objectFlow.setTarget(targetDecisionNode);
        this.activityEdgeHelper.updateActivityEdgeContainer(objectFlow);
        assertEquals(List.of(objectFlow), activity.getEdges());
    }

    /**
     * Tests that the activity container of an edge is updated if its source and
     * target elements are contained in another {@link Activity}.
     */
    @Test
    public void testUpdateActivityEdgeContainerSourceAndTargetInOtherActivity() {
        ObjectFlow objectFlow = this.create(ObjectFlow.class);
        DecisionNode sourceDecisionNode = this.create(DecisionNode.class);
        DecisionNode targetDecisionNode = this.create(DecisionNode.class);
        Activity edgeActivity = this.create(Activity.class);
        edgeActivity.getEdges().add(objectFlow);
        Activity nodesActivity = this.create(Activity.class);
        sourceDecisionNode.setActivity(nodesActivity);
        targetDecisionNode.setActivity(nodesActivity);
        objectFlow.setSource(sourceDecisionNode);
        objectFlow.setTarget(targetDecisionNode);
        assertTrue(nodesActivity.getEdges().isEmpty());
        this.activityEdgeHelper.updateActivityEdgeContainer(objectFlow);
        assertEquals(List.of(objectFlow), nodesActivity.getEdges());
        assertTrue(edgeActivity.getEdges().isEmpty());
    }

    /**
     * Tests that the activity container of an edge is updated if its source and
     * target elements are contained in another {@link StructuredActivityNode}.
     */
    @Test
    public void testUpdateActivityEdgeContainerSourceAndTargetInOtherStructuredActivityNode() {
        ObjectFlow objectFlow = this.create(ObjectFlow.class);
        DecisionNode sourceDecisionNode = this.create(DecisionNode.class);
        DecisionNode targetDecisionNode = this.create(DecisionNode.class);
        Activity edgeActivity = this.create(Activity.class);
        edgeActivity.getEdges().add(objectFlow);
        StructuredActivityNode structuredActivityNode = this.create(StructuredActivityNode.class);
        structuredActivityNode.getNodes().add(sourceDecisionNode);
        structuredActivityNode.getNodes().add(targetDecisionNode);
        objectFlow.setSource(sourceDecisionNode);
        objectFlow.setTarget(targetDecisionNode);
        assertTrue(structuredActivityNode.getEdges().isEmpty());
        this.activityEdgeHelper.updateActivityEdgeContainer(objectFlow);
        assertEquals(List.of(objectFlow), structuredActivityNode.getEdges());
        assertTrue(edgeActivity.getEdges().isEmpty());
    }

    /**
     * Tests that the activity container of an edge isn't updated if its source is
     * {@code null}.
     */
    @Test
    public void testUpdateActivityEdgeContainerNullSource() {
        ObjectFlow objectFlow = this.create(ObjectFlow.class);
        DecisionNode targetDecisionNode = this.create(DecisionNode.class);
        Activity edgeActivity = this.create(Activity.class);
        edgeActivity.getEdges().add(objectFlow);
        StructuredActivityNode structuredActivityNode = this.create(StructuredActivityNode.class);
        structuredActivityNode.getNodes().add(targetDecisionNode);
        objectFlow.setSource(null);
        objectFlow.setTarget(targetDecisionNode);
        assertTrue(structuredActivityNode.getEdges().isEmpty());
        this.activityEdgeHelper.updateActivityEdgeContainer(objectFlow);
        assertEquals(List.of(objectFlow), edgeActivity.getEdges());
        assertTrue(structuredActivityNode.getEdges().isEmpty());
    }

}
