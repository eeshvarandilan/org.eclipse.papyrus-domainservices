/*****************************************************************************
 * Copyright (c) 2012, 2023 CEA LIST, Christian W. Damus, Esterel Technologies SAS, EclipseSource and others.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Camille Letavernier (CEA LIST) camille.letavernier@cea.fr - Initial API and implementation
 *  Christian W. Damus (CEA) - Handle dynamic profile applications in CDO
 *  Christian W. Damus - bug 399859
 *  Christian W. Damus - bug 451613
 *  Christian W. Damus - bug 474610
 *  Calin Glitia (Esterel Technologies SAS) - bug 497699
 *  Camille Letavernier (EclipseSource) - bug 530156
 *  Nicolas FAUVERGUE (CEA LIST) nicolas.fauvergue@cea.fr - Bug 538193
 *
 *****************************************************************************/
package org.eclipse.papyrus.uml.domain.services.profile;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import org.eclipse.uml2.uml.AttributeOwner;
import org.eclipse.uml2.uml.Classifier;
import org.eclipse.uml2.uml.Operation;
import org.eclipse.uml2.uml.OperationOwner;
import org.eclipse.uml2.uml.Package;
import org.eclipse.uml2.uml.PackageableElement;
import org.eclipse.uml2.uml.Parameter;
import org.eclipse.uml2.uml.Profile;
import org.eclipse.uml2.uml.Property;

/**
 * A class containing static utility method regarding UML profiles.<br/>
 * 
 * Copied from {@link org.eclipse.papyrus.uml.tools.utils.ProfileUtil}
 *
 * @author Camille Letavernier
 */
public class ProfileUtil extends org.eclipse.uml2.uml.util.UMLUtil {

    /**
     * Sort a list of profiles in dependency order, such that profiles that are used
     * by other profiles sort ahead of the profiles that use them. A profile uses
     * another profile when it has classifiers that either:
     * <ul>
     * <li>specialize classifiers contained in the other profile (possibly in nested
     * packages), or</li>
     * <li>have attributes typed by classifiers in the other profile, or</li>
     * <li>have operations with parameters typed by classifiers in the other
     * profile</li>
     * </ul>
     *
     * @param profiles
     *                 a list of profiles to sort in place
     */
    public static void sortProfiles(List<Profile> profiles) {
        // Analyze profiles for inter-dependencies
        final Map<Profile, Set<Profile>> dependencies = computeProfileDependencies(profiles);

        // Expand to find transitive dependencies
        expand(dependencies);

        // Now sort by used profiles ahead of those that use them
        Collections.sort(profiles, new Comparator<Profile>() {
            @Override
            public int compare(Profile o1, Profile o2) {
                int compareResult = 0;
                if (dependencies.get(o1).contains(o2)) {
                    compareResult = 1;
                } else if (dependencies.get(o2).contains(o1)) {
                    compareResult = -1;
                }
                return compareResult;
            }
        });
    }

    /**
     * Compute a mapping of profiles to the profiles that they depend on.
     *
     * @param profiles
     *                 profiles for which to compute dependencies
     * @return the dependency mapping (one-to-many)
     */
    private static Map<Profile, Set<Profile>> computeProfileDependencies(Collection<? extends Profile> profiles) {
        Map<Profile, Set<Profile>> result = new LinkedHashMap<>();
        for (Profile next : profiles) {
            for (PackageableElement member : next.getPackagedElements()) {
                if (member instanceof Classifier) {
                    // Look for supertype dependencies
                    for (Classifier general : ((Classifier) member).allParents()) {
                        addProfileContaining(general, next, result);
                    }
                }
                if (member instanceof AttributeOwner) {
                    // Look for attribute type dependencies
                    for (Property property : ((AttributeOwner) member).getOwnedAttributes()) {
                        addProfileContaining(property.getType(), next, result);
                    }
                }
                if (member instanceof OperationOwner) {
                    // Look for operation parameter type dependencies
                    for (Operation operation : ((OperationOwner) member).getOwnedOperations()) {
                        for (Parameter parameter : operation.getOwnedParameters()) {
                            addProfileContaining(parameter.getType(), next, result);
                        }
                    }
                }
            }

            // Exclude self dependencies
            result.get(next).remove(next);
        }

        return result;
    }

    private static void addProfileContaining(PackageableElement element, Profile dependent,
            Map<Profile, Set<Profile>> dependencies) {
        if (element != null) {
            Package containingPackage = element.getNearestPackage();
            while ((containingPackage != null) && !(containingPackage instanceof Profile)
                    && (containingPackage.getOwner() != null)) {
                containingPackage = containingPackage.getOwner().getNearestPackage();
            }

            if (containingPackage instanceof Profile) {
                Set<Profile> containingProfiles = Optional.of(dependencies.get(dependent))
                        .orElse(new LinkedHashSet<>());
                containingProfiles.add((Profile) containingPackage);
                dependencies.put(dependent, containingProfiles);
            }
        }
    }

    /**
     * Expands a profile dependency map to include all transitive dependencies in
     * the mapping for each profile.
     *
     * @param dependencies
     *                     a dependency map to expand
     */
    private static void expand(Map<Profile, Set<Profile>> dependencies) {
        boolean changed = true;
        while (changed) {
            changed = false;

            for (Profile next : new ArrayList<>(dependencies.keySet())) {
                for (Profile dep : new ArrayList<>(dependencies.get(next))) {
                    Set<Profile> transitive = dependencies.get(dep);

                    // Add the transitive dependencies to my own
                    if ((transitive != null) && dependencies.get(next).addAll(transitive)) {
                        changed = true;
                    }
                }
            }
        }
    }
}
