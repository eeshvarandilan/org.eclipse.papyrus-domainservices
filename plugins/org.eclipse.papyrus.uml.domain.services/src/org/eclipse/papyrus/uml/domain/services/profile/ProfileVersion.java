/*****************************************************************************
 * Copyright (c) 2023 CEA LIST.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Chokri Mraidha (CEA LIST) Chokri.Mraidha@cea.fr - Initial API and implementation
 *  Patrick Tessier (CEA LIST) Patrick.Tessier@cea.fr - modification
 *
 *****************************************************************************/
package org.eclipse.papyrus.uml.domain.services.profile;

import java.util.NoSuchElementException;
import java.util.StringTokenizer;

/**
 * Representation of the version number for a profile.<br/>
 * 
 * Copied from {@link org.eclipse.papyrus.uml.tools.profile.definition.Version}
 * 
 * @author Chokri Mraidha
 */
public class ProfileVersion {

    /**
     * The empty version "0.0.0". Equivalent to calling.
     * <code>new Version(0,0,0)</code>
     */
    public static final ProfileVersion EMPTY_VERSION = new ProfileVersion(0, 0, 0);

    /** separator for the version string. */
    private static final String SEPARATOR = ".";

    /** major version number. */
    private int major;

    /** minor version number. */
    private int minor;

    /** micro version number. */
    private int micro;

    /**
     * Creates a new Version.
     *
     * @param major
     *              the major version value (should be positive)
     * @param minor
     *              the minor version value (should be positive)
     * @param micro
     *              the micro version value (should be positive)
     */
    public ProfileVersion(int major, int minor, int micro) {
        this.major = major;
        this.minor = minor;
        this.micro = micro;
    }

    /**
     * Creates a new Version, parsing a string value.
     *
     * @param value
     *              the string representing the version
     */
    public ProfileVersion(String value) throws IllegalArgumentException {
        try {
            StringTokenizer st = new StringTokenizer(value, SEPARATOR, true);
            major = Integer.parseInt(st.nextToken());

            if (st.hasMoreTokens()) {
                st.nextToken(); // consume delimiter
                minor = Integer.parseInt(st.nextToken());

                if (st.hasMoreTokens()) {
                    st.nextToken(); // consume delimiter
                    micro = Integer.parseInt(st.nextToken());

                    if (st.hasMoreTokens()) {
                        throw new IllegalArgumentException("invalid format");
                    }
                }
            }
        } catch (NoSuchElementException e) {
            throw new IllegalArgumentException("invalid format");
        }
    }

    /**
     * Returns the major version number.
     *
     * @return The major version number
     */
    public int getMajor() {
        return major;
    }

    /**
     * Returns the minor version number.
     *
     * @return The minor version number
     */
    public int getMinor() {
        return minor;
    }

    /**
     * Returns the micro version number.
     *
     * @return The micro version number
     */
    public int getMicro() {
        return micro;
    }

//    /**
//     * Updates the version numbers.
//     *
//     * @param major
//     *              the new major value
//     * @param minor
//     *              the new minor value
//     * @param micro
//     *              the new micro value
//     */
//    public void updateVersion(int newMajor, int newMinor, int newMicro) {
//        this.major = newMajor;
//        this.minor = newMinor;
//        this.micro = newMicro;
//    }

    /**
     * Creates a version given the specific String.
     *
     * @param version
     *                the string to parse
     * @return the version value corresponding to the String
     */
    public static ProfileVersion parseVersion(String version) throws IllegalArgumentException {
        if (version == null || version.trim().length() == 0) {
            return EMPTY_VERSION;
        }

        return new ProfileVersion(version);
    }

    /**
     * Returns the string corresponding to the version.
     */
    @Override
    public String toString() {
        return major + SEPARATOR + minor + SEPARATOR + micro;
    }
}
