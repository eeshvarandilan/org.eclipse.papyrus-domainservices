/*****************************************************************************
 * Copyright (c) 2023 CEA LIST, Obeo.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Obeo - Initial API and implementation
 *****************************************************************************/
package org.eclipse.papyrus.uml.domain.services.drop.diagrams;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.papyrus.uml.domain.services.status.CheckStatus;
import org.eclipse.uml2.uml.Artifact;
import org.eclipse.uml2.uml.Classifier;
import org.eclipse.uml2.uml.Comment;
import org.eclipse.uml2.uml.Constraint;
import org.eclipse.uml2.uml.DeploymentSpecification;
import org.eclipse.uml2.uml.Device;
import org.eclipse.uml2.uml.ExecutionEnvironment;
import org.eclipse.uml2.uml.Node;
import org.eclipse.uml2.uml.Package;
import org.eclipse.uml2.uml.UMLPackage;
import org.eclipse.uml2.uml.util.UMLSwitch;

/**
 * A specific switch to verify the drop of Deployment Diagram concepts.
 * 
 * @author <a href="mailto:florian.barbin@obeo.fr">Florian Barbin</a>
 *
 */
public class DeploymentDropInsideRepresentationCheckerSwitch extends UMLSwitch<CheckStatus> {

    private final EObject newSemanticContainer;

    DeploymentDropInsideRepresentationCheckerSwitch(EObject target) {
        super();
        this.newSemanticContainer = target;
    }

    @Override
    public CheckStatus caseArtifact(Artifact artifact) {
        final CheckStatus result;
        if (this.newSemanticContainer instanceof Package
                || UMLPackage.eINSTANCE.getArtifact() == this.newSemanticContainer.eClass()
                || (this.newSemanticContainer instanceof Node && !(this.newSemanticContainer instanceof Device))) {
            result = CheckStatus.YES;
        } else {
            result = CheckStatus.no(
                    "Artifact can only be drag and drop on a Package, a Node (different from a Device), an Artifact.");
        }
        return result;
    }

    @Override
    public CheckStatus caseComment(Comment comment) {
        final CheckStatus result;
        if (!(this.newSemanticContainer instanceof Package)) {
            result = CheckStatus.no("Comment can only be drag and drop on a Package.");
        } else {
            result = CheckStatus.YES;
        }
        return result;
    }

    @Override
    public CheckStatus caseConstraint(Constraint constraint) {
        final CheckStatus result;
        if (!(this.newSemanticContainer instanceof Package)) {
            result = CheckStatus.no("Constraint can only be drag and drop on a Package.");
        } else {
            result = CheckStatus.YES;
        }
        return result;
    }

    @Override
    public CheckStatus caseDeploymentSpecification(DeploymentSpecification deploymentSpecification) {
        final CheckStatus result;
        if (this.newSemanticContainer instanceof Package || this.newSemanticContainer instanceof Classifier
                && !(this.newSemanticContainer instanceof DeploymentSpecification)) {
            result = CheckStatus.YES;
        } else {
            result = CheckStatus.no(
                    "DeploymentSpecification can only be drag and drop on a Package or a Classifier different from a DeploymentSpecification.");
        }
        return result;
    }

    @Override
    public CheckStatus caseExecutionEnvironment(ExecutionEnvironment executionEnvironment) {
        final CheckStatus result;
        if (this.newSemanticContainer instanceof Package || this.newSemanticContainer instanceof Node) {
            result = CheckStatus.YES;
        } else {
            result = CheckStatus.no("ExecutionEnvironment can only be drag and drop on a Package or a Node.");
        }
        return result;
    }

    @Override
    public CheckStatus caseNode(Node node) {
        // This rule is common to Node and Device.
        final CheckStatus result;
        if (this.newSemanticContainer instanceof Package || (this.newSemanticContainer instanceof Node
                && !(this.newSemanticContainer instanceof ExecutionEnvironment))) {
            result = CheckStatus.YES;
        } else {
            result = CheckStatus
                    .no("Node can only be drag and drop on a Package or a Node (but not ExecutionEnvironment).");
        }
        return result;
    }

    @Override
    public CheckStatus casePackage(Package pkg) {
        // This rule is common to Package and Model.
        final CheckStatus result;
        if (this.newSemanticContainer instanceof Package) {
            result = CheckStatus.YES;
        } else {
            result = CheckStatus.no("A Package can only be drag and drop on a Package.");
        }
        return result;
    }

    @Override
    public CheckStatus defaultCase(EObject object) {
        return CheckStatus.no("DnD is not authorized.");
    }
}
