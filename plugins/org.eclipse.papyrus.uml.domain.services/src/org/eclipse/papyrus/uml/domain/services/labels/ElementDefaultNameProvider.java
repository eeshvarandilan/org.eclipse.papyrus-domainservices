/*****************************************************************************
 * Copyright (c) 2009, 2023 CEA LIST and others.
 *
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Yann TANGUY (CEA LIST) yann.tanguy@cea.fr - Initial API and implementation
 *  Christian W. Damus (CEA) - bug 440263
 *  Vincent LORENZO (CEA LIST) vincent.lorenzo@cea.fr - bug 530155
 *  Obeo - Refactoring Papyrus Web
 *****************************************************************************/
package org.eclipse.papyrus.uml.domain.services.labels;

import java.util.Collection;
import java.util.Collections;
import java.util.Optional;

import org.eclipse.emf.ecore.ENamedElement;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.uml2.uml.ActivityEdge;
import org.eclipse.uml2.uml.Association;
import org.eclipse.uml2.uml.Class;
import org.eclipse.uml2.uml.Extension;
import org.eclipse.uml2.uml.GeneralOrdering;
import org.eclipse.uml2.uml.NamedElement;
import org.eclipse.uml2.uml.Pseudostate;
import org.eclipse.uml2.uml.ReadSelfAction;
import org.eclipse.uml2.uml.Relationship;
import org.eclipse.uml2.uml.Stereotype;
import org.eclipse.uml2.uml.Transition;
import org.eclipse.uml2.uml.util.UMLSwitch;

/**
 * Provider of a default names.
 *
 * @author Arthur Daussy
 */
public class ElementDefaultNameProvider {

    /**
     * Get a default name for an element.
     *
     * <p>
     * Same as {@link #getDefaultName(NamedElement, Collection)} but use the parent
     * content to define the context)
     * </p>
     * <p>
     * Be aware that if the element already has a name and is already contained in
     * the context list, the the current name is returned
     * </p>
     *
     * @param element
     *                the element to set the name on
     * @param parent
     *                defining the context for the default name computation
     * @return a name or <code>null</code> if the given element does not need a
     *         default name
     */
    public String getDefaultName(NamedElement element, EObject parent) {
        return this.getDefaultName(element, this.getContextContent(parent));
    }

    private Collection<EObject> getContextContent(EObject parent) {
        Collection<EObject> contents;
        if (parent != null) {
            contents = parent.eContents();
        } else {
            contents = Collections.emptyList();
        }
        return contents;
    }

    /**
     * Get a default name for an element.
     *
     * <p>
     * Be aware that if the element already has a name and is already contained in
     * the context list, the the current name is returned
     * </p>
     *
     * @param element
     *                the element to set the name on
     * @param context
     *                the list of other children in which the element will be stored
     * @return a name or <code>null</code> if the given element does not need a
     *         default name
     */
    // CHECKSTYLE:OFF From Papyrus
    // org.eclipse.papyrus.uml.tools.utils.NamedElementUtil.computeDefaultNameWithIncrementFromBase(String,
    // Collection<?>, EObject, String, String)
    public String getDefaultName(NamedElement element, Collection<EObject> context) {
        if (element == null) {
            return null;
        }

        // Do not change the name if it's already present in the contents collection and
        // already has a name
        if (element.getName() != null && context.contains(element)) {
            if (element instanceof ENamedElement) {
                ENamedElement eNamedElement = (ENamedElement) element;
                if (eNamedElement.getName() != null) {
                    return eNamedElement.getName();
                }
            }
            // UML specific
            NamedElement namedElement = element;
            if (namedElement.getName() != null) {
                return namedElement.getName();
            }
        }

        return new NameGenerator(context).doSwitch(element).orElse(null);
    }
    // CHECKSTYLE:ON

    final class NameGenerator extends UMLSwitch<Optional<String>> {

        private static final String EXTENSION_SEPARATOR = "_"; //$NON-NLS-1$

        private static final String EXTENSION_BASE = "E_"; //$NON-NLS-1$

        /**
         * Separator use between element name and its index.
         */
        private static final String SEPARATOR = ""; // $NON-NLS-1$

        private final Collection<EObject> context;

        NameGenerator(Collection<EObject> context) {
            super();
            this.context = context;
        }

        @Override
        public Optional<String> caseRelationship(Relationship object) {
            // Do not given a name to association by default
            return Optional.empty();
        }

        @Override
        public Optional<String> caseAssociation(Association object) {
            // Do not given a name to association by default
            return Optional.empty();
        }

        @Override
        public Optional<String> caseActivityEdge(ActivityEdge object) {
            // Do not given a name to ActivityEdge by default
            return Optional.empty();
        }

        @Override
        public Optional<String> caseExtension(Extension extension) {
            Stereotype source = extension.getStereotype();
            Class target = extension.getMetaclass();
            String name = EXTENSION_BASE;
            name += source.getName() + EXTENSION_SEPARATOR + target.getName();
            name = this.computeDefaultNameWithIncrementFromBase(name, extension, SEPARATOR);
            return Optional.of(name);
        }

        @Override
        public Optional<String> caseTransition(Transition object) {
            // Do not given a name to Transition by default
            return Optional.empty();
        }

        @Override
        public Optional<String> caseGeneralOrdering(GeneralOrdering object) {
            // Do not given a name to GeneralOrdering by default
            return Optional.empty();
        }

        @Override
        public Optional<String> caseReadSelfAction(ReadSelfAction object) {
            // A ReadSelfAction should always be named "this".
            return Optional.of("this"); //$NON-NLS-1$
        }

        @Override
        public Optional<String> defaultCase(EObject object) {
            String base = object.eClass().getName();
            return Optional.ofNullable(this.computeDefaultNameWithIncrementFromBase(base, object, SEPARATOR));
        }

        @Override
        public Optional<String> casePseudostate(Pseudostate pseudostate) {
            String base = pseudostate.getKind().getLiteral();
            base = base.substring(0, 1).toUpperCase() + base.substring(1);

            return Optional.of(this.computeDefaultNameWithIncrementFromBase(base, pseudostate, SEPARATOR));
        }

        private String computeDefaultNameWithIncrementFromBase(String base, EObject elementToRename, String separator) {

            // @formatter:off
             int nextNumber = this.context.stream()
                     // If the element is already stored in the container and has a name
                     .filter(e -> e != elementToRename && e instanceof NamedElement)
                     .map(o -> ((NamedElement) o).getName())
                     .filter(name -> name != null && name.startsWith(base))
                     .map(name -> name.substring(base.length() + separator.length()))
                     .mapToInt(end -> {
                         try {
                             return Integer.parseInt(end);
                         } catch (NumberFormatException ex) {
                             return 0;
                         }
                     }).max().orElse(0) + 1;

             // @formatter:on

            return base + separator + nextNumber;
        }

    }

}
