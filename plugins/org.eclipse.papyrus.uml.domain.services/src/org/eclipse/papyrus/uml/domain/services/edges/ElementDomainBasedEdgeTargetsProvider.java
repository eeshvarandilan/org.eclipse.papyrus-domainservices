/*****************************************************************************
 * Copyright (c) 2022, 2023 CEA LIST, Obeo.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Obeo - Initial API and implementation
 *****************************************************************************/
package org.eclipse.papyrus.uml.domain.services.edges;

import static java.util.stream.Collectors.toList;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.papyrus.uml.domain.services.internal.helpers.OccurrenceSpecificationHelper;
import org.eclipse.uml2.uml.ActivityEdge;
import org.eclipse.uml2.uml.Association;
import org.eclipse.uml2.uml.Classifier;
import org.eclipse.uml2.uml.Component;
import org.eclipse.uml2.uml.ComponentRealization;
import org.eclipse.uml2.uml.Connector;
import org.eclipse.uml2.uml.ConnectorEnd;
import org.eclipse.uml2.uml.Dependency;
import org.eclipse.uml2.uml.Deployment;
import org.eclipse.uml2.uml.DeploymentTarget;
import org.eclipse.uml2.uml.Extend;
import org.eclipse.uml2.uml.Extension;
import org.eclipse.uml2.uml.Generalization;
import org.eclipse.uml2.uml.Include;
import org.eclipse.uml2.uml.InformationFlow;
import org.eclipse.uml2.uml.Interface;
import org.eclipse.uml2.uml.InterfaceRealization;
import org.eclipse.uml2.uml.Manifestation;
import org.eclipse.uml2.uml.Message;
import org.eclipse.uml2.uml.MessageEnd;
import org.eclipse.uml2.uml.OccurrenceSpecification;
import org.eclipse.uml2.uml.PackageImport;
import org.eclipse.uml2.uml.PackageMerge;
import org.eclipse.uml2.uml.PackageableElement;
import org.eclipse.uml2.uml.Substitution;
import org.eclipse.uml2.uml.Transition;
import org.eclipse.uml2.uml.Type;
import org.eclipse.uml2.uml.UseCase;
import org.eclipse.uml2.uml.util.UMLSwitch;

/**
 * Computes the semantic target of a domain based edge.
 *
 * @author <a href="mailto:arthur.daussy@obeo.fr">Arthur Daussy</a>
 */
public class ElementDomainBasedEdgeTargetsProvider implements IDomainBasedEdgeTargetsProvider {

    @Override
    public List<? extends EObject> getTargets(EObject semanticElement) {
        return new EdgeProviderSwitch().doSwitch(semanticElement);
    }

    private static class EdgeProviderSwitch extends UMLSwitch<List<? extends EObject>> {

        private List<? extends EObject> adaptOptionalSingleton(EObject value) {
            if (value != null) {
                return Collections.singletonList(value);
            } else {
                return Collections.emptyList();
            }
        }

        @Override
        public List<? extends EObject> caseActivityEdge(ActivityEdge activityEdge) {
            return List.of(activityEdge.getTarget());
        }

        @Override
        public List<? extends EObject> caseAssociation(Association object) {
            // Target of association is the type of the source property
            Type type = null;
            if (object.getMemberEnds().size() > 0) {
                type = object.getMemberEnds().get(0).getType();
            }
            if (type != null) {
                return Collections.singletonList(type);
            } else {
                return Collections.emptyList();
            }
        }

        @Override
        public List<? extends EObject> caseComponentRealization(ComponentRealization componentRealization) {
            Component abstraction = componentRealization.getAbstraction();
            if (abstraction != null) {
                return Collections.singletonList(abstraction);
            } else {
                return Collections.emptyList();
            }
        }

        @Override
        public List<? extends EObject> caseConnector(Connector connector) {
            EList<ConnectorEnd> ends = connector.getEnds();
            return ends.stream().skip(1)//
                    .map(end -> end.getRole())//
                    .collect(toList());
        }

        @Override
        public List<? extends EObject> caseDependency(Dependency object) {
            return List.copyOf(object.getSuppliers());
        }

        @Override
        public List<? extends EObject> caseDeployment(Deployment deployment) {
            DeploymentTarget deploymentTarget = deployment.getLocation();
            if (deploymentTarget != null) {
                return Collections.singletonList(deploymentTarget);
            } else {
                return Collections.emptyList();
            }
        }

        @Override
        public List<? extends EObject> caseExtend(Extend extend) {
            UseCase useCase = extend.getExtendedCase();
            if (useCase != null) {
                return Collections.singletonList(useCase);
            } else {
                return Collections.emptyList();
            }
        }

        @Override
        public List<? extends EObject> caseExtension(Extension extension) {
            //@formatter:off
            return Optional.ofNullable(extension.getMetaclass())
                    .map(List::of)
                    .orElse(List.of());
            //@formatter:on
        }

        @Override
        public List<? extends EObject> caseGeneralization(Generalization generatization) {
            Classifier general = generatization.getGeneral();
            if (general != null) {
                return Collections.singletonList(general);
            } else {
                return Collections.emptyList();
            }
        }

        @Override
        public List<? extends EObject> caseInclude(Include include) {
            UseCase useCase = include.getAddition();
            if (useCase != null) {
                return Collections.singletonList(useCase);
            } else {
                return Collections.emptyList();
            }
        }

        @Override
        public List<? extends EObject> caseInformationFlow(InformationFlow informationFlow) {
            return List.copyOf(informationFlow.getInformationTargets());
        }

        @Override
        public List<? extends EObject> caseInterfaceRealization(InterfaceRealization interfaceRealization) {
            Interface contract = interfaceRealization.getContract();
            if (contract != null) {
                return Collections.singletonList(contract);
            } else {
                return Collections.emptyList();
            }
        }

        @Override
        public List<? extends EObject> caseManifestation(Manifestation manifestation) {
            PackageableElement utilizedElement = manifestation.getUtilizedElement();
            if (utilizedElement != null) {
                return Collections.singletonList(utilizedElement);
            } else {
                return Collections.emptyList();
            }
        }

        @Override
        public List<? extends EObject> caseMessage(Message message) {
            MessageEnd receiveEvent = message.getReceiveEvent();
            if (receiveEvent instanceof OccurrenceSpecification) {
                return Collections.singletonList(
                        OccurrenceSpecificationHelper.findEnclosingElement((OccurrenceSpecification) receiveEvent));
            }
            return Collections.emptyList();
        }

        @Override
        public List<? extends EObject> casePackageImport(PackageImport object) {
            return this.adaptOptionalSingleton(object.getImportedPackage());
        }

        @Override
        public List<? extends EObject> casePackageMerge(PackageMerge object) {
            return this.adaptOptionalSingleton(object.getMergedPackage());
        }

        @Override
        public List<? extends EObject> caseSubstitution(Substitution substitution) {
            Classifier contract = substitution.getContract();
            if (contract != null) {
                return Collections.singletonList(contract);
            } else {
                return Collections.emptyList();
            }
        }

        @Override
        public List<? extends EObject> caseTransition(Transition transition) {
            return List.of(transition.getTarget());
        }

        @Override
        public List<EObject> defaultCase(EObject object) {
            return List.of();
        }
    }
}
