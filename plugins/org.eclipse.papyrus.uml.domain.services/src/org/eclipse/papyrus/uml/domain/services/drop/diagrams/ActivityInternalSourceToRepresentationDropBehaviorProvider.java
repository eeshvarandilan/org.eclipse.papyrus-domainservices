/*****************************************************************************
 * Copyright (c) 2023 CEA LIST, Obeo.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Obeo - Initial API and implementation
 *****************************************************************************/
package org.eclipse.papyrus.uml.domain.services.drop.diagrams;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.util.ECrossReferenceAdapter;
import org.eclipse.papyrus.uml.domain.services.IEditableChecker;
import org.eclipse.papyrus.uml.domain.services.drop.IInternalSourceToRepresentationDropBehaviorProvider;
import org.eclipse.papyrus.uml.domain.services.internal.helpers.ActivityEdgeHelper;
import org.eclipse.papyrus.uml.domain.services.modify.ElementFeatureModifier;
import org.eclipse.papyrus.uml.domain.services.modify.IFeatureModifier;
import org.eclipse.papyrus.uml.domain.services.status.State;
import org.eclipse.papyrus.uml.domain.services.status.Status;
import org.eclipse.uml2.uml.Activity;
import org.eclipse.uml2.uml.ActivityEdge;
import org.eclipse.uml2.uml.ActivityGroup;
import org.eclipse.uml2.uml.ActivityNode;
import org.eclipse.uml2.uml.ActivityPartition;
import org.eclipse.uml2.uml.Element;
import org.eclipse.uml2.uml.InterruptibleActivityRegion;
import org.eclipse.uml2.uml.StructuredActivityNode;
import org.eclipse.uml2.uml.UMLPackage;
import org.eclipse.uml2.uml.util.UMLSwitch;

/**
 * Drop behavior provider of a diagram element to an Activity Diagram Element.
 *
 * @author <a href="mailto:glenn.plouhinec@obeo.fr">Glenn Plouhinec</a>
 */
public class ActivityInternalSourceToRepresentationDropBehaviorProvider
        implements IInternalSourceToRepresentationDropBehaviorProvider {

    @Override
    public Status drop(EObject droppedElement, EObject oldContainer, EObject newContainer,
            ECrossReferenceAdapter crossRef, IEditableChecker editableChecker) {
        return new ActivityDropOutsideRepresentationBehaviorProviderSwitch(oldContainer, newContainer, crossRef,
                editableChecker).doSwitch(droppedElement);
    }

    static class ActivityDropOutsideRepresentationBehaviorProviderSwitch extends UMLSwitch<Status> {

        private final EObject oldContainer;

        private final EObject newContainer;

        private final ECrossReferenceAdapter crossRef;

        private final IEditableChecker editableChecker;

        ActivityDropOutsideRepresentationBehaviorProviderSwitch(EObject oldContainer, EObject newContainer,
                ECrossReferenceAdapter crossRef, IEditableChecker editableChecker) {
            super();
            this.oldContainer = oldContainer;
            this.newContainer = newContainer;
            this.crossRef = crossRef;
            this.editableChecker = editableChecker;
        }

        /**
         * When dropping an ActivityPartition:
         * <ul>
         * <li>if the container is an Activity, the Activity#ownedGroup feature should
         * be modified.</li>
         * <li>if the container is an ActivityPartition, the
         * ActivityPartition#subPartition feature should be modified.</li>
         * <li>all nodes and edges referenced by the ActivityPartition should change
         * their Activity owner if it changed.</li>
         * </ul>
         *
         * @see org.eclipse.uml2.uml.util.UMLSwitch#caseActivityPartition(org.eclipse.uml2.uml.ActivityPartition)
         *
         * @param droppedActivityPartition
         *                                 the activityPartition to drop
         * @return OK or Failing status according to the complete D&D.
         */
        @Override
        public Status caseActivityPartition(ActivityPartition droppedActivityPartition) {
            Status dropStatus;
            IFeatureModifier modifier = new ElementFeatureModifier(this.crossRef, this.editableChecker);
            if (this.oldContainer != this.newContainer) {
                if (this.newContainer instanceof Activity newActivity) {
                    dropStatus = this.updateActivityPartitionContent(droppedActivityPartition, newActivity);
                    if (State.DONE == dropStatus.getState()) {
                        dropStatus = modifier.addValue(newActivity,
                                UMLPackage.eINSTANCE.getActivity_Partition().getName(), droppedActivityPartition);
                    }
                } else if (this.newContainer instanceof ActivityPartition newPartition) {
                    Activity newActivityContainer = this.findActivity(newPartition);
                    if (this.oldContainer != newActivityContainer) {
                        dropStatus = this.updateActivityPartitionContent(droppedActivityPartition,
                                newActivityContainer);
                    } else {
                        // The partition is moved to another partition contained in the same Activity,
                        // there is nothing to do semantically for its elements.
                        dropStatus = Status.DONE;
                    }
                    if (State.DONE == dropStatus.getState()) {
                        dropStatus = modifier.addValue(newPartition,
                                UMLPackage.eINSTANCE.getActivityPartition_Subpartition().getName(),
                                droppedActivityPartition);
                    }
                } else {
                    dropStatus = Status.createFailingStatus(
                            "An ActivityPartition can only be dropped in an Activity or ActivityPartition");
                }
                return dropStatus;
            }
            return super.caseActivityPartition(droppedActivityPartition);
        }

        /**
         * Finds the elements that are referenced by the provided
         * {@code activityPartition} and are contained in the
         * {@code activityPartition}'s {@link Activity} container.
         * <p>
         * This method returns elements directly referenced by the provided
         * {@code activityPartition} and elements referenced by sub-partitions.
         * </p>
         *
         * @param activityPartition
         *                          the {@link ActivityPartition} to retrieve the
         *                          elements from
         *
         * @return a record wrapping the {@link ActivityNode} and {@link ActivityEdge}
         *         elements
         */
        private ActivityPartitionContent findPartitionContentContainedInActivity(ActivityPartition activityPartition) {
            // Retrieve the Activity containing the partition to drop. We can use
            // getInActivity() here because it returns null if the partition to drop is
            // contained in another partition.
            final Activity partitionActivity = this.findActivity(activityPartition);
            ActivityPartitionContent partitionContent = new ActivityPartitionContent(new ArrayList<>(),
                    new ArrayList<>());
            partitionContent.addPartitionContent(activityPartition, partitionActivity);
            for (ActivityPartition subpartition : activityPartition.getSubpartitions()) {
                partitionContent.addAll(this.findPartitionContentContainedInActivity(subpartition));
            }
            return partitionContent;
        }

        /**
         * Updates the Activity container of elements referenced by the
         * ActivityPartition and its sub-partitions.
         *
         * @param droppedActivityPartition
         *                                 the ActivityPartition to drop
         * @param newActivity
         *                                 the new Activity container
         * @return OK or Failing status according to the FeatureModifier status
         */
        private Status updateActivityPartitionContent(ActivityPartition droppedActivityPartition,
                Activity newActivity) {
            IFeatureModifier modifier = new ElementFeatureModifier(this.crossRef, this.editableChecker);
            ActivityPartitionContent allPartitionContent = this
                    .findPartitionContentContainedInActivity(droppedActivityPartition);
            Status newStatus = Status.createOKStatus(droppedActivityPartition);
            for (ActivityNode node : allPartitionContent.activityNodes) {
                if (State.FAILED != newStatus.getState()) {
                    newStatus = modifier.addValue(newActivity, UMLPackage.eINSTANCE.getActivity_OwnedNode().getName(),
                            node);
                } else {
                    break;
                }
            }
            for (ActivityEdge edge : allPartitionContent.activityEdges) {
                if (State.FAILED != newStatus.getState()) {
                    newStatus = modifier.addValue(newActivity, UMLPackage.eINSTANCE.getActivity_Edge().getName(), edge);
                } else {
                    break;
                }
            }
            return newStatus;
        }

        /**
         * Defines an ActivityPartitionContent, which contains two Set referencing all
         * ActivityNodes and ActivityEdges displayed in an ActivityPartition. These
         * elements are contained by the Activity of the ActivityPartition.
         */
        record ActivityPartitionContent(List<ActivityNode> activityNodes, List<ActivityEdge> activityEdges) {
            /**
             * Add all the content of another ActivityPartitionContent.
             *
             * @param partitionContent
             *                         the ActivityPartitionContent to be added
             */
            public void addAll(ActivityPartitionContent partitionContent) {
                this.activityNodes.addAll(partitionContent.activityNodes);
                this.activityEdges.addAll(partitionContent.activityEdges);
            }

            /**
             * Adds the contents of an ActivityPartition.
             *
             * @param partition
             *                          the ActivityPartition to be added
             * @param partitionActivity
             *                          the activity containing the partition
             */
            public void addPartitionContent(ActivityPartition partition, Activity partitionActivity) {
                List<ActivityNode> nodesContainedInActivity = partition.getNodes().stream()
                        .filter(node -> node.getOwner().equals(partitionActivity)) //
                        .collect(Collectors.toList());
                List<ActivityEdge> edgesContainedInActivity = partition.getEdges().stream()
                        .filter(edge -> edge.getOwner().equals(partitionActivity)) //
                        .collect(Collectors.toList());
                this.activityNodes.addAll(nodesContainedInActivity);
                this.activityEdges.addAll(edgesContainedInActivity);
            }
        }

        /**
         * When dropping an ActivityNode:
         * <ul>
         * <li>if the container is an Activity, the Activity#ownedNode feature should be
         * modified.</li>
         * <li>if the container is an ActivityPartition or an
         * InterruptibleActivityRegion, the Activity container should be modified and
         * the ActivityPartition#node or InterruptibleActivityRegion#node feature should
         * also be modified.</li>
         * <li>if the container is a StructuredActivityNode, the
         * StructuredActivityNode#node feature should be modified.</li>
         * </ul>
         *
         * @see org.eclipse.uml2.uml.util.UMLSwitch#caseActivityNode(org.eclipse.uml2.uml.ActivityNode)
         *
         * @param droppedActivityNode
         *                            the activityNode to drop
         * @return OK or Failing status according to the complete D&D.
         */
        @Override
        public Status caseActivityNode(ActivityNode droppedActivityNode) {
            if (this.oldContainer != this.newContainer) {
                return this.handleActivityNode(droppedActivityNode, UMLPackage.eINSTANCE.getActivity_OwnedNode());
            }
            return super.caseActivityNode(droppedActivityNode);
        }

        private Status handleActivityNode(ActivityNode droppedActivityNode,
                EStructuralFeature parentActivityContainmentFeature) {
            IFeatureModifier modifier = new ElementFeatureModifier(this.crossRef, this.editableChecker);
            Status dropStatus;
            if (this.oldContainer instanceof Activity) {
                dropStatus = modifier.removeValue(this.oldContainer, parentActivityContainmentFeature.getName(),
                        droppedActivityNode);
            } else if (this.oldContainer instanceof ActivityGroup
                    && !(this.oldContainer instanceof StructuredActivityNode)) {
                dropStatus = this.removeActivityNodeInActivityGroup((ActivityGroup) this.oldContainer,
                        droppedActivityNode);
            } else if (this.oldContainer instanceof StructuredActivityNode) {
                dropStatus = modifier.removeValue(this.oldContainer,
                        UMLPackage.eINSTANCE.getStructuredActivityNode_Node().getName(), droppedActivityNode);
            } else {
                dropStatus = Status.createFailingStatus(MessageFormat.format(
                        "The old container could not be modified, its type is not managed when dragging and dropping {0}.",
                        droppedActivityNode.eClass().getName()));
            }
            if (State.DONE == dropStatus.getState()) {
                if (this.newContainer instanceof Activity) {
                    dropStatus = modifier.addValue(this.newContainer, parentActivityContainmentFeature.getName(),
                            droppedActivityNode);
                } else if (this.newContainer instanceof ActivityGroup
                        && !(this.newContainer instanceof StructuredActivityNode)) {
                    dropStatus = this.addActivityNodeInActivityGroup((ActivityGroup) this.newContainer,
                            droppedActivityNode, parentActivityContainmentFeature);
                } else if (this.newContainer instanceof StructuredActivityNode) {
                    dropStatus = modifier.addValue(this.newContainer,
                            UMLPackage.eINSTANCE.getStructuredActivityNode_Node().getName(), droppedActivityNode);
                } else {
                    dropStatus = Status.createFailingStatus(MessageFormat.format(
                            "The new container could not be modified, its type is not managed when dragging and dropping {0}.",
                            droppedActivityNode.eClass().getName()));
                }
            }
            this.updateActivityEdgesFromActivityNode(droppedActivityNode, dropStatus);
            return dropStatus;
        }

        /**
         * Default Behavior : UML element can be D&D by using the same reference
         * containment.
         *
         * @see org.eclipse.uml2.uml.util.UMLSwitch#caseElement(org.eclipse.uml2.uml.Element)
         *
         * @param droppedElement
         *                       the element to drop
         * @return OK or Failing status according to the complete D&D.
         */
        @Override
        public Status caseElement(Element droppedElement) {
            Status dropStatus;
            IFeatureModifier modifier = new ElementFeatureModifier(this.crossRef, this.editableChecker);
            if (this.oldContainer != this.newContainer) {
                String refName = droppedElement.eContainmentFeature().getName();
                if (this.oldContainer.eClass().getEStructuralFeature(refName) != null
                        && this.newContainer.eClass().getEStructuralFeature(refName) != null) {
                    dropStatus = modifier.removeValue(this.oldContainer, refName, droppedElement);
                    if (State.DONE == dropStatus.getState()) {
                        dropStatus = modifier.addValue(this.newContainer, refName, droppedElement);
                    }
                    return dropStatus;
                }
            }
            return super.caseElement(droppedElement);
        }

        @Override
        public Status caseStructuredActivityNode(StructuredActivityNode droppedStructuredActivityNode) {
            if (this.oldContainer != this.newContainer) {
                return this.handleActivityNode(droppedStructuredActivityNode,
                        UMLPackage.eINSTANCE.getActivity_StructuredNode());
            }
            return super.caseStructuredActivityNode(droppedStructuredActivityNode);
        }

        /**
         * This method is used to update activityEdges associated to the
         * droppedActivityNode.
         *
         * @param droppedActivityNode
         *                            the dropped activityNode
         * @param dropStatus
         *                            the Status of the drop operation
         */
        private void updateActivityEdgesFromActivityNode(ActivityNode droppedActivityNode, Status dropStatus) {
            if (State.DONE == dropStatus.getState()) {
                ActivityEdgeHelper activityEdgeHelper = new ActivityEdgeHelper();
                Set<ActivityEdge> allEdges = activityEdgeHelper
                        .getAllActivityEdgesFromActivityNode(droppedActivityNode);
                for (ActivityEdge activityEdge : allEdges) {
                    activityEdgeHelper.updateActivityEdgeContainer(activityEdge);
                    activityEdgeHelper.setInPartition(activityEdge);
                }
            }
        }

        /**
         * This method is used to remove an ActivityNode from an ActivityPartition or an
         * InterruptibleActivityRegion.
         *
         * @param container
         *                            the ActivityPartition or
         *                            InterruptibleActivityRegion graphic container
         * @param droppedActivityNode
         *                            the element to drop
         * @return OK or Failing status according to the complete D&D.
         */
        private Status removeActivityNodeInActivityGroup(ActivityGroup container, ActivityNode droppedActivityNode) {
            Status dropStatus;
            Activity parentActivity = this.findActivity(container);
            IFeatureModifier modifier = new ElementFeatureModifier(this.crossRef, this.editableChecker);
            if (parentActivity != null) {
                dropStatus = modifier.removeValue(parentActivity,
                        UMLPackage.eINSTANCE.getActivity_OwnedNode().getName(), droppedActivityNode);
                String featureName = null;
                if (State.DONE == dropStatus.getState()) {
                    if (container instanceof ActivityPartition) {
                        featureName = UMLPackage.eINSTANCE.getActivityPartition_Node().getName();
                    } else if (container instanceof InterruptibleActivityRegion) {
                        featureName = UMLPackage.eINSTANCE.getInterruptibleActivityRegion_Node().getName();
                    }
                    if (featureName != null) {
                        modifier.removeValue(container, featureName, droppedActivityNode);
                    }
                }
            } else {
                dropStatus = Status.createFailingStatus(MessageFormat.format(
                        "The {0} \"{1}\" container doesn't have any associated Activity container.",
                        container.eClass().getName(), container.getName()));
            }
            return dropStatus;
        }

        /**
         * This method is used to add an ActivityNode from an ActivityPartition or an
         * InterruptibleActivityRegion.
         *
         * @param container
         *                                         the ActivityPartition or
         *                                         InterruptibleActivityRegion graphic
         *                                         container
         * @param droppedActivityNode
         *                                         the element to drop
         * @param parentActivityContainmentFeature
         *                                         the containment feature that will own
         *                                         the dropped {@link ActivityNode} in
         *                                         the parent {@link Activity}.
         * @return OK or Failing status according to the complete D&D.
         */
        private Status addActivityNodeInActivityGroup(ActivityGroup container, ActivityNode droppedActivityNode,
                EStructuralFeature parentActivityContainmentFeature) {
            Status dropStatus;
            Activity parentActivity = this.findActivity(container);
            IFeatureModifier modifier = new ElementFeatureModifier(this.crossRef, this.editableChecker);
            if (parentActivity != null) {
                dropStatus = modifier.addValue(parentActivity, parentActivityContainmentFeature.getName(),
                        droppedActivityNode);
                String featureName = null;
                if (State.DONE == dropStatus.getState()) {
                    if (container instanceof ActivityPartition) {
                        featureName = UMLPackage.eINSTANCE.getActivityPartition_Node().getName();
                    } else if (container instanceof InterruptibleActivityRegion) {
                        featureName = UMLPackage.eINSTANCE.getInterruptibleActivityRegion_Node().getName();
                    }
                    if (featureName != null) {
                        modifier.addValue(container, featureName, droppedActivityNode);
                    }
                }
            } else {
                dropStatus = Status.createFailingStatus(MessageFormat.format(
                        "The {0} \"{1}\" container doesn't have any associated Activity container.",
                        container.eClass().getName(), container.getName()));
            }
            return dropStatus;
        }

        /**
         * Default case: the Drag and Drop is not allowed.
         *
         * @see org.eclipse.uml2.uml.util.UMLSwitch#defaultCase(org.eclipse.emf.ecore.EObject)
         *
         * @param object
         *               the element to drop
         * @return Failing status.
         */
        @Override
        public Status defaultCase(EObject object) {
            return Status.createFailingStatus("Drag and Drop operation could not be performed.");
        }

        /**
         * Find parent Activity.
         *
         * @param editElement
         *                    the activityGroup element or activity container
         * @return the Activity container if it exists; <code>null</code> otherwise.
         */
        private Activity findActivity(EObject editElement) {
            Activity container = null;
            if (editElement instanceof ActivityGroup) {
                ActivityGroup activityGroup = (ActivityGroup) editElement;
                if (activityGroup.eContainer() instanceof Activity) {
                    container = (Activity) activityGroup.eContainer();
                } else {
                    container = this.findActivity(activityGroup.eContainer());
                }
            }
            return container;
        }
    }
}
