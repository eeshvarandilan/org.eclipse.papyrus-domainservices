/*****************************************************************************
 * Copyright (c) 2023 CEA LIST, Obeo.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Obeo - Initial API and implementation
 *****************************************************************************/
package org.eclipse.papyrus.uml.domain.services.properties;

import java.text.MessageFormat;
import java.util.List;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EEnumLiteral;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.papyrus.uml.domain.services.properties.ILogger.ILogLevel;

/**
 * Services used in Stereotype application page.
 *
 * @author Jerome Gout
 */
public class PropertiesStereotypeApplicationServices {

    private static final String BOOLEAN_OBJECT_UNSET_LITERAL = "null";

    private static final String BOOLEAN_OBJECT_TRUE_LITERAL = "true";

    private static final String BOOLEAN_OBJECT_FALSE_LITERAL = "false";

    private ILogger logger;

    public PropertiesStereotypeApplicationServices(ILogger logger) {
        this.logger = logger;
    }

    public List<EStructuralFeature> getAllFeatures(EObject target) {
        return target.eClass().getEAllStructuralFeatures().stream().filter(this::isValidFeature).toList();
    }

    private boolean isValidFeature(EStructuralFeature feature) {
        return !feature.getName().startsWith("base_") && !feature.isTransient() && !feature.isDerived() && !this.isContainmentReference(feature);
    }

    private boolean isContainmentReference(EStructuralFeature feature) {
        if (feature instanceof EReference reference) {
            return reference.isContainment();
        }
        return false;
    }

    public boolean isEditable(EStructuralFeature feature) {
        return feature.isChangeable() && !feature.isTransient();
    }

    public Object getStereotypeFeatureValue(EObject stereotypeApplication, EStructuralFeature feature) {
        return stereotypeApplication.eGet(feature);
    }

    public Object getStereotypeEnumerationValue(EObject stereotypeApplication, EStructuralFeature feature) {
        var value = stereotypeApplication.eGet(feature);
        if (value instanceof EEnumLiteral literal) {
            return literal.getLiteral();
        }
        return null;
    }

    public Object getStereotypeBooleanObjectValue(EObject stereotypeApplication, EStructuralFeature feature) {
        if (this.isMonoBooleanObjectAttribute(feature)) {
            var value = stereotypeApplication.eGet(feature);
            if (value != null) {
                return value.toString();
            }
        }
        return BOOLEAN_OBJECT_UNSET_LITERAL;
    }

    private void setIntegerValue(EObject stereotypeApplication, EStructuralFeature feature, String value) {
        try {
            Integer number = Integer.valueOf(value);
            stereotypeApplication.eSet(feature, number);
        } catch (NumberFormatException e) {
            this.logger.log(MessageFormat.format("''{0}'' is not a valid value for feature ''{1}'' expecting an integer number", value, feature.getName()), ILogLevel.ERROR);
        }
    }

    private void setDoubleValue(EObject stereotypeApplication, EStructuralFeature feature, String value) {
        try {
            Double number = Double.valueOf(value);
            stereotypeApplication.eSet(feature, number);
        } catch (NumberFormatException e) {
            this.logger.log(MessageFormat.format("''{0}'' is not a valid value for feature ''{1}'' expecting a double number", value, feature.getName()), ILogLevel.ERROR);
        }
    }

    private void setFloatValue(EObject stereotypeApplication, EStructuralFeature feature, String value) {
        try {
            Float number = Float.valueOf(value);
            stereotypeApplication.eSet(feature, number);
        } catch (NumberFormatException e) {
            this.logger.log(MessageFormat.format("''{0}'' is not a valid value for feature ''{1}'' expecting a float number", value, feature.getName()), ILogLevel.ERROR);
        }
    }

    public Object setStereotypeFeatureValue(EObject stereotypeApplication, EStructuralFeature feature, Object value) {
        if (this.isMonoIntegerAttribute(feature)) {
            this.setIntegerValue(stereotypeApplication, feature, (String) value);
        } else if (this.isMonoDoubleAttribute(feature)) {
            this.setDoubleValue(stereotypeApplication, feature, (String) value);
        } else if (this.isMonoFloatAttribute(feature)) {
            this.setFloatValue(stereotypeApplication, feature, (String) value);
        } else if (this.isMonoBooleanAttribute(feature)) {
            stereotypeApplication.eSet(feature, value);
        } else if (this.isMonoBooleanObjectAttribute(feature)) {
            stereotypeApplication.eSet(feature, this.getBooleanObjectLiteral((String) value));
        } else if (this.isMonoStringAttribute(feature)) {
            stereotypeApplication.eSet(feature, value);
        } else if (this.isMonoEnumeration(feature)) {
            stereotypeApplication.eSet(feature, this.getEnumLiteral(feature, (String) value));
        }
        return stereotypeApplication;
    }

    private EEnumLiteral getEnumLiteral(EStructuralFeature feature, String value) {
        if (feature instanceof EAttribute attr && !attr.isMany()) {
            EDataType type = attr.getEAttributeType();
            if (type instanceof EEnum enumeration) {
                return enumeration.getEEnumLiteral(value);
            }
        }
        return null;
    }

    public boolean isMonoStringAttribute(EStructuralFeature feature) {
        if (feature instanceof EAttribute attr && !attr.isMany()) {
            return this.isAttributeOfType(attr, String.class);
        }
        return false;
    }

    public boolean isMultiStringAttribute(EStructuralFeature feature) {
        if (feature instanceof EAttribute attr && attr.isMany()) {
            return this.isAttributeOfType(attr, String.class);
        }
        return false;
    }

    public boolean isMonoBooleanAttribute(EStructuralFeature feature) {
        if (feature instanceof EAttribute attr && !attr.isMany()) {
            return this.isAttributeOfType(attr, boolean.class);
        }
        return false;
    }

    public boolean isMultiBooleanAttribute(EStructuralFeature feature) {
        if (feature instanceof EAttribute attr && attr.isMany()) {
            return this.isAttributeOfType(attr, boolean.class) || this.isAttributeOfType(attr, Boolean.class);
        }
        return false;
    }

    public boolean isMonoBooleanObjectAttribute(EStructuralFeature feature) {
        if (feature instanceof EAttribute attr && !attr.isMany()) {
            return this.isAttributeOfType(attr, Boolean.class);
        }
        return false;
    }

    public boolean isMonoIntegerAttribute(EStructuralFeature feature) {
        if (feature instanceof EAttribute attr && !attr.isMany()) {
            return this.isAttributeOfType(attr, int.class) || this.isAttributeOfType(attr, Integer.class);
        }
        return false;
    }

    public boolean isMultiIntegerAttribute(EStructuralFeature feature) {
        if (feature instanceof EAttribute attr && attr.isMany()) {
            return this.isAttributeOfType(attr, int.class) || this.isAttributeOfType(attr, Integer.class);
        }
        return false;
    }

    public boolean isMonoDoubleAttribute(EStructuralFeature feature) {
        if (feature instanceof EAttribute attr && !attr.isMany()) {
            return this.isAttributeOfType(attr, double.class) || this.isAttributeOfType(attr, Double.class);
        }
        return false;
    }

    public boolean isMultiDoubleAttribute(EStructuralFeature feature) {
        if (feature instanceof EAttribute attr && attr.isMany()) {
            return this.isAttributeOfType(attr, double.class) || this.isAttributeOfType(attr, Double.class);
        }
        return false;
    }

    public boolean isMonoFloatAttribute(EStructuralFeature feature) {
        if (feature instanceof EAttribute attr && !attr.isMany()) {
            return this.isAttributeOfType(attr, float.class) || this.isAttributeOfType(attr, Float.class);
        }
        return false;
    }

    public boolean isMultiFloatAttribute(EStructuralFeature feature) {
        if (feature instanceof EAttribute attr && attr.isMany()) {
            return this.isAttributeOfType(attr, float.class) || this.isAttributeOfType(attr, Float.class);
        }
        return false;
    }

    private boolean isAttributeOfType(EAttribute attr, Class<?> expectedType) {
        EDataType type = attr.getEAttributeType();
        if (type != null) {
            Class<?> instanceClass = type.getInstanceClass();
            if (instanceClass != null) {
                return instanceClass.isAssignableFrom(expectedType);
            }
        }
        return false;
    }

    private boolean isReferenceOfType(EReference ref, Class<?> expectedType) {
        var type = ref.getEReferenceType();
        if (type != null) {
            return type.getClass().isAssignableFrom(expectedType);
        }
        return false;
    }

    public boolean isMonoEnumeration(EStructuralFeature feature) {
        if (feature instanceof EAttribute attr && !attr.isMany()) {
            EDataType type = attr.getEAttributeType();
            if (type instanceof EEnum) {
                return true;
            }
        }
        return false;
    }

    public boolean isMultiEnumeration(EStructuralFeature feature) {
        if (feature instanceof EAttribute attr && attr.isMany()) {
            EDataType type = attr.getEAttributeType();
            if (type instanceof EEnum) {
                return true;
            }
        }
        return false;
    }

    public List<String> getStereotypeEnumerationLiterals(EStructuralFeature feature) {
        if (feature instanceof EAttribute attr) {
            EDataType type = attr.getEAttributeType();
            if (type instanceof EEnum enumeration) {
                return enumeration.getELiterals().stream().map(EEnumLiteral::getLiteral).toList();
            }
        }
        return List.of();
    }

    public List<String> getStereotypeBooleanObjectLiterals(EStructuralFeature feature) {
        return List.of(BOOLEAN_OBJECT_TRUE_LITERAL, BOOLEAN_OBJECT_FALSE_LITERAL, BOOLEAN_OBJECT_UNSET_LITERAL);
    }

    private Boolean getBooleanObjectLiteral(String value) {
        if (BOOLEAN_OBJECT_UNSET_LITERAL.equals(value)) {
            return null;
        }
        return Boolean.valueOf(value);
    }

}
