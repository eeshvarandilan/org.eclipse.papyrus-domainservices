/*****************************************************************************
 * Copyright (c) 2023 CEA LIST, Obeo.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Obeo - Initial API and implementation
 *****************************************************************************/

package org.eclipse.papyrus.uml.domain.services.scope;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Stream;

import org.eclipse.emf.common.notify.Notifier;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.papyrus.uml.domain.services.EMFUtils;
import org.eclipse.uml2.uml.Element;
import org.eclipse.uml2.uml.Namespace;
import org.eclipse.uml2.uml.Package;

/**
 * Service to compute the list of root element in which candidate for a
 * reference can be located.
 *
 * @author <a href="mailto:arthur.daussy@obeo.fr">Arthur Daussy</a>
 */
public class ElementRootCandidateSeachProvider implements IRootCandidateSearchProvider {

    @Override
    public List<Notifier> getReachableRoots(EObject self) {
        return computeReachableRootCandidates(self);
    }

    private List<Notifier> computeReachableRootCandidates(EObject self) {
        final List<Notifier> result;
        if (self instanceof Element elem) {
            result = computeElementReachableRoots(elem);
        } else if (self != null && isStereotypeApplication(self)) {
            result = handleStereotypeApplication(self);
        } else {
            result = List.of();
        }
        return result;
    }

    private List<Notifier> handleStereotypeApplication(EObject self) {
        final List<Notifier> result;
        Element element = getBaseElement(self);
        if (element != null) {
            result = computeElementReachableRoots(element);
        } else {
            result = List.of();
        }
        return result;
    }

    private Element getBaseElement(EObject self) {
        return (Element) getBaseReference(self).map(ref -> self.eGet(ref)).orElse(null);
    }

    private boolean isStereotypeApplication(EObject self) {
        return getBaseReference(self).isPresent();
    }

    private Optional<EReference> getBaseReference(EObject self) {
        return self.eClass().getEAllReferences().stream()
                .filter(ref -> ref.getName().startsWith("base_") && !ref.isMany() && self.eGet(ref) instanceof Element)
                .findFirst();
    }

    private void computeReachableRoots(Element elem, Set<Notifier> roots, Set<Notifier> computed) {
        computed.add(elem);
        for (Package importedPackage : getAllImportedPackages(elem)) {
            if (!computed.contains(importedPackage) && roots.add(importedPackage)) {
                computeReachableRoots(importedPackage, roots, computed);
            }
        }
    }

    private List<Notifier> computeElementReachableRoots(Element elem) {
        Set<Notifier> roots = new HashSet<>();
        Set<Notifier> computed = new HashSet<>();
        EObject rootElement = EcoreUtil.getRootContainer(elem);
        if (rootElement != null) {
            roots.add(rootElement);
            computed.add(rootElement);
        }
        for (Package importedPackage : getAllImportedPackages(elem)) {
            if (roots.add(importedPackage)) {
                computeReachableRoots(importedPackage, roots, computed);
            }
        }
        return filterNestedRoots(roots);
    }

    /**
     * Remove nested root to only keep high level roots
     * 
     * @param roots
     *              the non filtered list of root
     * @return the filtered list of root
     * 
     */
    private List<Notifier> filterNestedRoots(Set<Notifier> roots) {
        List<Notifier> result = new ArrayList<>(roots);
        Iterator<Notifier> iterator = result.iterator();
        while (iterator.hasNext()) {
            Object root = iterator.next();
            if (root instanceof EObject rootEObject) {

                EObject eContainer = rootEObject.eContainer();

                boolean isNested = false;
                while (eContainer != null && !isNested) {
                    isNested = roots.contains(eContainer);
                    eContainer = eContainer.eContainer();
                }

                if (isNested) {
                    iterator.remove();
                }
            }
        }
        return result;
    }

    private List<Package> getAllImportedPackages(Element elem) {
        List<Namespace> ancestors = EMFUtils.getAncestors(Namespace.class, elem);
        Stream<Package> importedNamespace = ancestors.stream().flatMap(pack -> pack.getImportedPackages().stream());
        if (elem instanceof Package pack) {
            importedNamespace = Stream.concat(importedNamespace, pack.getImportedPackages().stream());
        }
        return importedNamespace.distinct().toList();
    }

}
