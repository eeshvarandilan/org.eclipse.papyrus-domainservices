/*****************************************************************************
 * Copyright (c) 2023 CEA LIST, Obeo.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Obeo - Initial API and implementation
 *****************************************************************************/
package org.eclipse.papyrus.uml.domain.services.drop.diagrams;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.papyrus.uml.domain.services.drop.IExternalSourceToRepresentationDropChecker;
import org.eclipse.papyrus.uml.domain.services.status.CheckStatus;
import org.eclipse.uml2.uml.Classifier;
import org.eclipse.uml2.uml.Comment;
import org.eclipse.uml2.uml.Component;
import org.eclipse.uml2.uml.Constraint;
import org.eclipse.uml2.uml.Interface;
import org.eclipse.uml2.uml.Operation;
import org.eclipse.uml2.uml.Package;
import org.eclipse.uml2.uml.Port;
import org.eclipse.uml2.uml.Property;
import org.eclipse.uml2.uml.Reception;
import org.eclipse.uml2.uml.Type;
import org.eclipse.uml2.uml.util.UMLSwitch;

/**
 * Checks if a semantic D&D is possible from the Model Explorer to the Component
 * diagram.
 *
 * @author <a href="mailto:gwendal.daniel@obeosoft.com">Gwendal Daniel</a>
 */
public class ComponentExternalSourceToRepresentationDropChecker implements IExternalSourceToRepresentationDropChecker {

    @Override
    public CheckStatus canDragAndDrop(EObject elementToDrop, EObject newSemanticContainer) {
        return new ComponentDropInsideRepresentationCheckerSwitch(newSemanticContainer).doSwitch(elementToDrop);
    }

    static class ComponentDropInsideRepresentationCheckerSwitch extends UMLSwitch<CheckStatus> {

        private final EObject newSemanticContainer;

        ComponentDropInsideRepresentationCheckerSwitch(EObject target) {
            super();
            this.newSemanticContainer = target;
        }

        @Override
        public CheckStatus caseComment(Comment comment) {
            final CheckStatus result;
            if (this.newSemanticContainer instanceof Package) {
                result = CheckStatus.YES;
            } else {
                result = CheckStatus.no("Comment can only be drag and drop on a Package");
            }
            return result;
        }

        @Override
        public CheckStatus caseComponent(Component component) {
            final CheckStatus result;
            if (this.newSemanticContainer instanceof Component || this.newSemanticContainer instanceof Package
                    || this.newSemanticContainer instanceof Property) {
                result = CheckStatus.YES;
            } else {
                result = CheckStatus.no("Component can only be drag and drop on a Component, Package, or Property");
            }
            return result;
        }

        @Override
        public CheckStatus caseConstraint(Constraint constraint) {
            final CheckStatus result;
            if (this.newSemanticContainer instanceof Package) {
                result = CheckStatus.YES;
            } else {
                result = CheckStatus.no("Constraint can only be drag and drop on a Package");
            }
            return result;
        }

        @Override
        public CheckStatus caseInterface(Interface interfaceElement) {
            final CheckStatus result;
            if (this.newSemanticContainer instanceof Package || this.newSemanticContainer instanceof Property) {
                result = CheckStatus.YES;
            } else {
                result = CheckStatus.no("Interface can only be drag and drop on a Package or Property");
            }
            return result;
        }

        @Override
        public CheckStatus caseOperation(Operation operation) {
            final CheckStatus result;
            if (this.newSemanticContainer instanceof Interface) {
                result = CheckStatus.YES;
            } else {
                result = CheckStatus.no("Operation can only be drag and drop on an Interface");
            }
            return result;
        }

        @Override
        public CheckStatus casePackage(Package pack) {
            // This method handles both Package and Model instances
            final CheckStatus result;
            if (this.newSemanticContainer instanceof Package) {
                result = CheckStatus.YES;
            } else {
                result = CheckStatus.no("Package can only be drag and drop on a Package");
            }
            return result;
        }

        @Override
        public CheckStatus casePort(Port port) {
            final CheckStatus result;
            if (this.newSemanticContainer instanceof Component || this.newSemanticContainer instanceof Property) {
                result = CheckStatus.YES;
            } else {
                result = CheckStatus.no("Port can only be drag and drop on Component or Property");
            }
            return result;
        }

        @Override
        public CheckStatus caseProperty(Property property) {
            CheckStatus result = CheckStatus
                    .no("Property can only be drag and drop on a Component, Interface, or typed Property");
            if (this.newSemanticContainer instanceof Classifier
                    && ((Classifier) this.newSemanticContainer).getAllAttributes().contains(property)) {
                result = CheckStatus.YES;
            } else if (this.newSemanticContainer instanceof Property) {
                Type type = ((Property) this.newSemanticContainer).getType();
                if (type instanceof Classifier && ((Classifier) type).getAllAttributes().contains(property)) {
                    result = CheckStatus.YES;
                }
            }
            return result;
        }

        @Override
        public CheckStatus caseReception(Reception reception) {
            final CheckStatus result;
            if (this.newSemanticContainer instanceof Interface) {
                result = CheckStatus.YES;
            } else {
                result = CheckStatus.no("Reception can ony be drag and drop on an Interface");
            }
            return result;
        }

        @Override
        public CheckStatus caseType(Type type) {
            final CheckStatus result;
            // Any Type can be dropped on a Property to type it.
            if (this.newSemanticContainer instanceof Property) {
                result = CheckStatus.YES;
            } else {
                result = CheckStatus.no("Type can only be drag and drop on a Property");
            }
            return result;
        }

        @Override
        public CheckStatus defaultCase(EObject object) {
            return CheckStatus.no("DnD is not authorized.");
        }

    }

}
