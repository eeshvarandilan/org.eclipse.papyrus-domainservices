/*****************************************************************************
 * Copyright (c) 2023 CEA LIST, OBEO
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  OBEO - Initial API and implementation
 *****************************************************************************/
package org.eclipse.papyrus.uml.domain.services.drop.diagrams;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.papyrus.uml.domain.services.drop.IExternalSourceToRepresentationDropChecker;
import org.eclipse.papyrus.uml.domain.services.status.CheckStatus;
import org.eclipse.uml2.uml.Class;
import org.eclipse.uml2.uml.Comment;
import org.eclipse.uml2.uml.Constraint;
import org.eclipse.uml2.uml.DataType;
import org.eclipse.uml2.uml.ElementImport;
import org.eclipse.uml2.uml.Enumeration;
import org.eclipse.uml2.uml.EnumerationLiteral;
import org.eclipse.uml2.uml.Operation;
import org.eclipse.uml2.uml.Package;
import org.eclipse.uml2.uml.PrimitiveType;
import org.eclipse.uml2.uml.Profile;
import org.eclipse.uml2.uml.Property;
import org.eclipse.uml2.uml.Stereotype;
import org.eclipse.uml2.uml.util.UMLSwitch;

/**
 * Checks if a semantic D&D is possible from the Model Explorer to the Profile
 * diagram.
 *
 * @author <a href="mailto:florian.barbin@obeo.fr">Florian Barbin</a>
 */
public class ProfileExternalSourceToRepresentationDropChecker implements IExternalSourceToRepresentationDropChecker {

    @Override
    public CheckStatus canDragAndDrop(EObject elementToDrop, EObject newSemanticContainer) {
        return new ProfileDropInsideRepresentationCheckerSwitch(newSemanticContainer).doSwitch(elementToDrop);
    }

    static class ProfileDropInsideRepresentationCheckerSwitch extends UMLSwitch<CheckStatus> {

        private final EObject newSemanticContainer;

        ProfileDropInsideRepresentationCheckerSwitch(EObject target) {
            super();
            this.newSemanticContainer = target;
        }

        @Override
        public CheckStatus caseClass(Class clazz) {
            return this.handlePackageContainer();
        }

        @Override
        public CheckStatus caseComment(Comment comment) {
            return this.handlePackageContainer();
        }

        @Override
        public CheckStatus caseConstraint(Constraint constraint) {
            return this.handlePackageContainer();
        }

        @Override
        public CheckStatus caseDataType(DataType dataType) {
            return this.handlePackageContainer();
        }

        @Override
        public CheckStatus caseElementImport(ElementImport elementImport) {
            final CheckStatus result;
            if (!(this.newSemanticContainer instanceof Profile)) {
                result = CheckStatus.no("An Element Import can only be drag and drop on a Profile.");
            } else {
                result = CheckStatus.YES;
            }
            return result;
        }

        @Override
        public CheckStatus caseEnumeration(Enumeration enumeration) {
            return this.handlePackageContainer();
        }

        @Override
        public CheckStatus caseEnumerationLiteral(EnumerationLiteral enumerationLiteral) {
            final CheckStatus result;
            if (!(this.newSemanticContainer instanceof Enumeration)) {
                result = CheckStatus.no("An Enumeration Literal can only be drag and drop on an Enumeration.");
            } else {
                result = CheckStatus.YES;
            }
            return result;
        }

        @Override
        public CheckStatus caseOperation(Operation object) {
            final CheckStatus result;
            // The Operation Drop is authorized over Class, DataType but not over
            // Enumeration.
            if ((!(this.newSemanticContainer instanceof org.eclipse.uml2.uml.Class)
                    && !(this.newSemanticContainer instanceof DataType))
                    || this.newSemanticContainer instanceof Enumeration) {
                result = CheckStatus.no("An Operation can only be drag and drop on a Class or a DataType.");
            } else {
                result = CheckStatus.YES;
            }
            return result;
        }

        @Override
        public CheckStatus casePackage(Package pack) {
            return this.handlePackageContainer();
        }

        @Override
        public CheckStatus casePrimitiveType(PrimitiveType primitiveType) {
            return this.handlePackageContainer();
        }

        @Override
        public CheckStatus caseProfile(Profile profile) {
            return this.handlePackageContainer();
        }

        @Override
        public CheckStatus caseProperty(Property property) {
            final CheckStatus result;
            // The Property Drop is authorized over Class, DataType but not over
            // Enumeration.
            if ((!(this.newSemanticContainer instanceof org.eclipse.uml2.uml.Class)
                    && !(this.newSemanticContainer instanceof DataType))
                    || this.newSemanticContainer instanceof Enumeration) {
                result = CheckStatus.no("A Property can only be drag and drop on a Class or a DataType.");
            } else {
                result = CheckStatus.YES;
            }
            return result;
        }

        @Override
        public CheckStatus caseStereotype(Stereotype stereotype) {
            return this.handlePackageContainer();
        }

        @Override
        public CheckStatus defaultCase(EObject object) {
            return CheckStatus.no("DnD is not authorized.");
        }

        private CheckStatus handlePackageContainer() {
            final CheckStatus result;
            if (!(this.newSemanticContainer instanceof Package)) {
                result = CheckStatus.no(String.format("{0} can only be drag and drop on a Package kind element.",
                        this.newSemanticContainer.eClass().getName()));
            } else {
                result = CheckStatus.YES;
            }
            return result;
        }
    }
}
